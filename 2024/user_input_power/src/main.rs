use std::io;

enum State
{
    Off,
    Sleep,
    Reboot,
    Shutdown,
    Hibernate
}
 
impl State
{
    fn new(state: &str)->Option<State>
    {
        let state = state.trim().to_lowercase();
        match state.as_str()
        {
            "off" => Some(State::Off),
            "sleep" => Some(State::Sleep),
            "reboot" => Some(State::Reboot),
            "shutdown" => Some(State::Shutdown),
            "hibernate" => Some(State::Hibernate),
            _ => None,
        }
    }
}

 fn print_power_action(state: State)
 {
     use State::*;
     match state
     {
        Off => println!("Turning off.."),
        Sleep => println!("Sleeping.."),
        Reboot => println!("Rebooting.."),
        Shutdown => println!("Shutting down.."),
        Hibernate => println!("Hibernating.."),
        _ => println!("Nothing found."),
     }
 }

fn main()
{
    let mut buffer = String::new(); 
    let user_input_state = io::stdin().read_line(&mut buffer);

    if user_input_state.is_ok()
    {
       match State::new(&buffer) 
       {
           Some(state) => print_power_action(state),
           None => println!("Invalid state."),
       }
    } else {
        println!("Error reading input.");
    }
}
