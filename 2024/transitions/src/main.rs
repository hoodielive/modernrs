#![allow(dead_code)]

// TypeStates

struct Employee<State>
{
    name: String,
    state: State,
}
impl Employee<Agreement>
{
    fn new(name: &str) -> Self
    {
        Self 
        {
            name: name.to_string(),
            state: Agreement,
        }
    }
}

struct Agreement;
impl<State> Employee<State>
{
    // If I borrow a state, I'd like for the existing state to remain:

   fn transition<NextState>(self, state: NextState) -> Employee<NextState>
   {
       Employee
       {
           name: self.name,
           state
       }
   }

   fn read_agreement(self) -> Employee<Signature>
   {
       // Sign the agreement.
       self.transition(Signature)
   }
}
struct Signature;
impl Employee<Signature>
{
    fn sign(self) -> Employee<Training>
    {
        self.transition(Training)
    }
}

struct Training;
#[rustfmt::skip]
impl Employee<Training>
{
    fn train(self, score: u8) -> Result<Employee<OnboardingComplete>, Employee<FailedTraining>>
    {
       if score >= 85
       {
           Ok(self.transition(OnboardingComplete { score }))
       }
       else
       {
           Err(self.transition(FailedTraining { score }))
       }
    }
}


struct FailedTraining { score: u8 }
struct OnboardingComplete { score: u8 }

fn main()
{
   let employee = Employee::new("Jim");
   let _onboarded = employee.read_agreement().sign().train(96);

    match _onboarded 
    {
        Ok(emp) => println!("Onboarding is completed because you passed the training."),
        Err(emp) => println!("You failed training with: {}", emp.state.score), }
}
