#![allow(dead_code)]

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
enum InitialPhase
{
    Init,
    ReceivedEgbe,
    ReceivedEgun,
    ReceivedIkofa,
    ReceivedOrisa,
    ReceivedIfa
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
struct Initiate<State>
{
    state: State,
    phase: InitialPhase, 
}

impl<State> Initiate<State>
{
    fn new(state: State) -> Self
    {
        Initiate
        {
            state,
            phase: InitialPhase::Init,
        }
    }

    // Define the transition method

    fn transition<NewState>(self, state: NewState, next_phase: InitialPhase) -> Initiate<NewState>
    {
        match next_phase
        {
            InitialPhase::ReceivedEgbe if self.phase == InitialPhase::Init =>
            {
                Initiate
                {
                    state,
                    phase: next_phase,
                }
            }

            InitialPhase::ReceivedEgun if self.phase == InitialPhase::ReceivedEgbe =>
            {
               Initiate 
               {
                   state,
                   phase: next_phase,
               }
            }
            InitialPhase::ReceivedIkofa if self.phase == InitialPhase::ReceivedEgun =>
            {
               Initiate 
               {
                   state,
                   phase: next_phase,
               }
            }
            InitialPhase::ReceivedOrisa if self.phase == InitialPhase::ReceivedIkofa =>
            {
               Initiate 
               {
                   state,
                   phase: next_phase,
               }
            }
            InitialPhase::ReceivedIfa if self.phase == InitialPhase::ReceivedOrisa =>
            {
               Initiate 
               {
                   state,
                   phase: next_phase,
               }
            }
        _ => {
                panic!("You are not initiated into shit.");
             }
        }// Ends match
    }

    fn received_egbe(self) -> Initiate<Egbe>
    {
        self.transition(Egbe, InitialPhase::ReceivedEgbe)
    }
    
    fn received_egun(self) -> Initiate<Egun>
    {
        self.transition(Egun, InitialPhase::ReceivedEgun)
    }

    fn received_ikofa(self) -> Initiate<Ikofa>
    {
        self.transition(Ikofa, InitialPhase::ReceivedIkofa)
    }

    fn received_orisa(self) -> Initiate<Orisa>
    {
        self.transition(Orisa, InitialPhase::ReceivedOrisa)
    }
    
    fn received_ifa(self) -> Initiate<Ifa>
    {
        self.transition(Ifa, InitialPhase::ReceivedIfa)
    }

} // Impl Ends

#[derive(Debug, Copy, Clone)] // bitch
struct Egbe;
#[derive(Debug, Copy, Clone)] // bitch
struct Egun;
#[derive(Debug, Copy, Clone)] // bitch
struct Ikofa;
struct Orisa;
struct Ifa;

fn main()
{
   let initiate = Initiate::new(Egbe);
   let initiated_with_egbe = initiate.received_egbe();
   let initiated_with_egun = initiated_with_egbe.received_egun();
   let initiated_with_ikofa = initiated_with_egun.received_ikofa();
   // println!("{:?}", initiated_with_egbe);
   println!("{:?}", initiated_with_ikofa);
}
