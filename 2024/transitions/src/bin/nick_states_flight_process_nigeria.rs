#![allow(dead_code)]

use std::fmt::Debug;

struct AtHome;
struct Packing;
#[derive(Debug)]
struct Airport;
struct CheckIn<Destination: Debug + Clone>
{
    destination: Destination
}
struct Boarding<Destination: Debug + Clone>
{
    destination: Destination
}
struct InFlight;
struct Arrived<Destination: Debug>
{
    destination: Destination
}

#[derive(Debug, Copy, Clone)]
struct FlightProcess<State, Destination>
{
    state: State,
    destination: Option<Destination>,
}

impl FlightProcess<AtHome, ()>
{
    fn pack_bags(self) -> FlightProcess<Packing, ()>
    {
        println!("Packing bags..");
        FlightProcess { state: Packing, destination: None }
    }
}

impl FlightProcess<Packing, ()>
{
    fn head_to_airport(self) -> FlightProcess<Airport, ()>
    {
        println!("Heading to the Airport.");
        FlightProcess { state: Airport, destination: None }
    }
}

impl FlightProcess<Airport, ()>
{
    fn head_to_airport<Destination>(self, destination: Option<Destination>) -> FlightProcess<CheckIn<Destination>, ()>
    {
        println!("Checking in for flight.");
        FlightProcess { state: CheckIn { destination: destination.clone() }, destination: None }
    }
}

impl<Destination: Debug + Clone> FlightProcess<CheckIn, ()>
{
    fn board_aircraft(self, destination: Option<Destination>) -> FlightProcess<Boarding<Destination>, Destination>
    {
        match destination 
        {
            Some(dest) => println!("Board the damn flight to {:?}!", dest),
            None => println!("Boarding the flight with no specific destination."),
        }
        
        FlightProcess { state: Boarding { destination: destination.clone() }, destination }
    }
}

impl<Destination: Debug> FlightProcess<Boarding, ()>
{
    fn fly(self, destined: String) -> FlightProcess<InFlight, ()>
    {
        println!("Fly to destination.");
            FlightProcess { state: InFlight, destination: Some()}
    }
}

impl<Destination: Debug> FlightProcess<InFlight<Destination>>
{
    fn arrive_to_destination<Destination: Debug>(self, destination: Destination) -> FlightProcess<Arrived, Destination>
    {
        println!("You have arrived to your final destination: {:?}", destination);
    }
}

fn main()
{
    
}
