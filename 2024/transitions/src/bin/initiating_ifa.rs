#![allow(dead_code)]

/** 
 * Receive Egun
 * Receive Egbe,
 * Receive Isefa
 * Olorisa
 * Itefa Itelodu
 **/

struct InitiationProcess<State>
{
    state: State,
}

struct Egun;
struct Egbe;
struct Isefa;
struct Olorisa;
struct Itefa;

impl InitiationProcess<Egun>
{
    fn receive_egun(self) -> InitiationProcess<Egbe>
    {
        println!("I have received a blessing from the Egun.");
        InitiationProcess { state: Egbe }
    }
}

impl InitiationProcess<Egbe>
{
    fn receive_egbe(self) -> InitiationProcess<Isefa>
    {
        println!("My egbe is now supporting me.");
        InitiationProcess { state: Isefa }
    }
}

impl InitiationProcess<Isefa>
{
    fn receive_isefa(self) -> InitiationProcess<Olorisa>
    {
        println!("I have received my first hand of ifa.");
        InitiationProcess { state: Olorisa }
    }
}

impl InitiationProcess<Olorisa>
{
    fn receive_orisa(self, orisa: &str) -> InitiationProcess<Itefa>
    {
        println!("I now have crowned by Orisa: {}", orisa);
        InitiationProcess { state: Itefa }
    }
}

impl InitiationProcess<Itefa>
{
    fn complete_itefa(self)
    {
        println!("I am now a proud Babalawo or Iyanifa.");
    }
}


fn main()
{
    // This is the first process.
    
   let initiation = InitiationProcess{ state: Egun };
    
   // Progress through my process.
    
   let egbe = initiation.receive_egun().receive_egbe();
   let isefa = egbe.receive_isefa();
   let olorisa = isefa.receive_orisa("Esu");
   olorisa.complete_itefa();
}
