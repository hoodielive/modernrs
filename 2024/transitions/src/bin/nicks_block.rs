#![allow(dead_code)]
#![allow(unused_imports)]

use std::time::{SystemTime, UNIX_EPOCH};
use sha2::{Digest, Sha256};
use itertools::Itertools;

struct Block
{
    index: u64,
    timestamp: u64,
    data: String,
    prev_hash: Vec<u8>,
    hash: Vec<u8>,
}

impl Block
{
    // Calculate the hash
    fn calculate_hash(&self) -> Vec<u8>
    {
        let header = format!("{}{}{}{}", 
             self.index, 
             self.timestamp, 
             &self.data,
             String::from_utf8_lossy(&self.prev_hash
        ));
        
        let mut hasher = Sha256::new();
        hasher.update(header);
        hasher.finalize().to_vec()
    }
    
    // Create a new block
    
    fn new(index: u64, timestamp: u64, data: String, prev_hash: Vec<u8>) -> Self
    {
        let hash = Self::calculate_hash(&Block { 
            index,
            timestamp,
            data: data.clone(),
            prev_hash: prev_hash.clone(),
            hash: vec![]
        });

        Self { index, timestamp, data, prev_hash, hash }
    }
}

struct Blockchain
{
    blocks: Vec<Block>,
}

impl Blockchain
{
    // Genesis block

    fn new() -> Self
    {
        let genesis_block = Block::new(0, 0, "Genesis Block".to_string(), vec![]);
        Self { blocks: vec![genesis_block] }
    }

    fn add_block(&mut self, data: String)
    {
        let prev_block = self.blocks.last().unwrap();
        let new_index = prev_block.index + 1;
        let new_timestamp = SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs();
        let new_hash = prev_block.hash.clone();
        let new_block = Block::new(new_index, new_timestamp, data, new_hash);
        self.blocks.push(new_block);
    }
}

fn main()
{
   let mut blockchain_for_nick = Blockchain::new();
   blockchain_for_nick.add_block("NickBlock 1 data".to_string());
   blockchain_for_nick.add_block("NickBlock 2 data".to_string());
   blockchain_for_nick.add_block("NickBlock 3 data".to_string());
   
   for block in &blockchain_for_nick.blocks
   {
       println!("Index: {}", block.index);
       println!("Timestamp: {}", block.timestamp);
       println!("Data: {}", block.data);
       println!("Previous Hash: {:x}", block.prev_hash.iter().format(""));
       println!("Hash: {:x}", block.hash.iter().format(""));
   }
}
