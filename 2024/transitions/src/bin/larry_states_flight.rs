#![allow(dead_code)]
#![allow(unused_imports)]

use std::fmt::Display;
use std::fmt::Debug;

enum FlightStates
{
    Init,
}

#[derive(Debug)]
struct AtHome;
struct Pack;
struct CheckIn;
struct Board;
struct InFlight;
struct Arrive;

#[derive(Debug)]
struct FlightProcess<State>
{
    state: State
}

impl<State> FlightProcess<State>
{
    fn new(state: State) -> Self
    {
        Self { state }
    }

    fn transition<NewState>(self, state: NewState) -> FlightProcess<NewState>
    {
        FlightProcess { state }
    }
}

impl FlightProcess<AtHome>
{
    fn athome(self) -> FlightProcess<AtHome>
    {
        FlightProcess { state: AtHome }
    }
}

impl FlightProcess<Pack>
{
    fn pack(self) -> FlightProcess<Pack>
    {
        FlightProcess { state: Pack }
    }
}

impl FlightProcess<CheckIn>
{
    fn checkin(self) -> FlightProcess<CheckIn>
    {
        FlightProcess { state: CheckIn }
    }
}

impl FlightProcess<Board>
{
    fn board(self) -> FlightProcess<Board>
    {
        FlightProcess { state: Board }
    }
}

impl FlightProcess<InFlight>
{
    fn inflight(self) -> FlightProcess<InFlight>
    {
        FlightProcess { state: InFlight }
    }
}

impl FlightProcess<Arrive>
{
    fn arrive(self) -> FlightProcess<Arrive>
    {
        FlightProcess { state: Arrive }
    }
}


fn main()
{
    let fly = FlightProcess::new(AtHome);    
    let home = fly.athome().pack();
    println!("You are {:?}", pack);
}
