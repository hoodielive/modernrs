use std::fmt::Debug;

// Define states for the flight process
struct AtHome;
struct Packing;
struct Airport;
struct CheckIn;
struct Boarding<Destination: Debug + Clone>; // Constrain Destination type
struct InFlight;
struct Arrived<Destination: Debug>;

// Main struct representing the flight process
struct FlightProcess<State, Destination> {
    state: State,
    destination: Option<Destination>,
}

impl FlightProcess<AtHome, ()> {
    fn pack_bags(self) -> FlightProcess<Packing, ()> {
        println!("Packing bags..");
        FlightProcess { state: Packing, destination: None }
    }
}

impl FlightProcess<Packing, ()> {
    fn head_to_airport(self) -> FlightProcess<Airport, ()> {
        println!("Heading to the Airport.");
        FlightProcess { state: Airport, destination: None }
    }
}

impl FlightProcess<Airport, ()> {
    fn check_in(self) -> FlightProcess<CheckIn, ()> {
        println!("Checking in for the flight.");
        FlightProcess { state: CheckIn, destination: None }
    }
}

impl<Destination: Debug + Clone> FlightProcess<CheckIn, ()> {
    fn board_aircraft(self, destination: Option<Destination>) -> FlightProcess<Boarding<Destination>, Destination> {
        match destination {
            Some(dest) => println!("Board the damn flight to {:?}!", dest),
            None => println!("Boarding the flight with no specific destination."),
        }
        FlightProcess { state: Boarding { destination: destination.clone() }, destination }
    }
}

impl<Destination: Debug> FlightProcess<Boarding<Destination>, Destination> {
    fn fly(self) -> FlightProcess<InFlight, Destination> {
        println!("Fly to destination.");
        FlightProcess { state: InFlight, destination: self.destination }
    }
}

impl<Destination: Debug> FlightProcess<InFlight, Destination> {
    fn arrive_to_destination(self) -> FlightProcess<Arrived<Destination>, Destination> {

// Define states for the flight process
struct AtHome;
struct Packing;
struct Airport;
struct CheckIn;
struct Boarding<Destination: Debug + Clone>; // Constrain Destination type
struct InFlight;
struct Arrived<Destination: Debug>;

// Main struct representing the flight process
struct FlightProcess<State, Destination> {
    state: State,
    destination: Option<Destination>,
}

impl FlightProcess<AtHome, ()> {
    fn pack_bags(self) -> FlightProcess<Packing, ()> {
        println!("Packing bags..");
        FlightProcess { state: Packing, destination: None }
    }
}

impl FlightProcess<Packing, ()> {
    fn head_to_airport(self) -> FlightProcess<Airport, ()> {
        println!("Heading to the Airport.");
        FlightProcess { state: Airport, destination: None }
    }
}

impl FlightProcess<Airport, ()> {
    fn check_in(self) -> FlightProcess<CheckIn, ()> {
        println!("Checking in for the flight.");
        FlightProcess { state: CheckIn, destination: None }
    }
}

impl<Destination: Debug + Clone> FlightProcess<CheckIn, ()> {
    fn board_aircraft(self, destination: Option<Destination>) -> FlightProcess<Boarding<Destination>, Destination> {
        match destination {
            Some(dest) => println!("Board the damn flight to {:?}!", dest),
            None => println!("Boarding the flight with no specific destination."),
        }
        FlightProcess { state: Boarding { destination: destination.clone() }, destination }
    }
}

impl<Destination: Debug> FlightProcess<Boarding<Destination>, Destination> {
    fn fly(self) -> FlightProcess<InFlight, Destination> {
        println!("Fly to destination.");
        FlightProcess { state: InFlight, destination: self.destination }
    }
}

impl<Destination: Debug> FlightProcess<InFlight, Destination> {
    fn arrive_to_destination(self) -> FlightProcess<Arrived<Destination>, Destination> {
        match self.destination {
            Some(dest) => println!("You have arrived to your final destination: {:?}", dest),
            None => println!("You have arrived to your final destination."),
        }
        FlightProcess { state: Arrived, destination: self.destination }
    }
}

fn main() {
    // Simulate the flight process
    let flight_process = FlightProcess { state: AtHome, destination: None };
    let packed_bags = flight_process.pack_bags();
    let heading_to_airport = packed_bags.head_to_airport();
    let checked_in = heading_to_airport.check_in();
    let boarded_flight = checked_in.board_aircraft(Some("Nigeria".to_string()));
    let flying = boarded_flight.fly();
    let arrived = flying.arrive_to_destination();
}
       match self.destination {
            Some(dest) => println!("You have arrived to your final destination: {:?}", dest),
            None => println!("You have arrived to your final destination."),
        }
        FlightProcess { state: Arrived, destination: self.destination }
    }
}

fn main() {
    // Simulate the flight process
    let flight_process = FlightProcess { state: AtHome, destination: None };
    let packed_bags = flight_process.pack_bags();
    let heading_to_airport = packed_bags.head_to_airport();
    let checked_in = heading_to_airport.check_in();
    let boarded_flight = checked_in.board_aircraft(Some("Nigeria".to_string()));
    let flying = boarded_flight.fly();
    let arrived = flying.arrive_to_destination();
}

