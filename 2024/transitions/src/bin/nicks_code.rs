#![allow(dead_code)]

// Process Initiating IFA.

struct Order<State>
{
    id: u32,
    state: State,
}

impl<State> Order<State>
{
   fn transition<NextState>(self, state: NextState) -> Order<NextState> 
   {
        Order
        {
            id: self.id,
            state
        }
   }

   fn process(self) -> Order<Processing>
   {
       self.transition(Processing)
   }
   
   fn completed(self) -> Order<Completed>
   {
       self.transition(Completed)
   }
}

struct Placed;
impl Order<Placed>
{
    fn new(id: u32) -> Self
    {
        Self
        {
            id,
            state: Placed,
        }
    }
}
struct Processing;
impl Order<Processing>
{
    fn resolve(self) -> Order<Completed>
    {
        self.completed()
    }

    fn cancel(self) -> Order<Cancelled>
    {
        self.transition(Cancelled)
    }
}
struct Completed;
struct Cancelled;

fn main()
{
   let order = Order::new(32);
   let processing_order = order.process();

   let resolved_order = processing_order.resolve();
   // or
   // let cancelled_order = processing_order.cancel();

   match resolved_order.state
   {
       Completed => println!("Your Order {} has been successfully resolved.", resolved_order.id),
       _ => println!("Your Order {} has been cancelled.", resolved_order.id),
   }
}
