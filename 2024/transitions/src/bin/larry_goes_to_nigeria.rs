#![allow(dead_code)]

use std::fmt::Debug;

trait Destination {}
struct AtHome;
struct Packing;
struct Airport;
struct Boarding<Destination: Debug + Clone>;
struct InFlight;
struct Arrived;

struct FlightProcess<State, Destination>
{
    state: State,
    destination: Option<Destination>,
}

impl FlightProcess<AtHome, ()>
{
    fn pack_bags(self) -> FlightProcess<Packing, ()>
    {
        println!("Packing bags..");
        FlightProcess { state: Packing, destination: None }
    }
}

impl FlightProcess<Packing>
{
    fn head_to_airport(self) -> FlightProcess<Airport>
    {
        println!("Heading to the Airport yo...");
        FlightProcess { state: Airport }
    }
}

impl FlightProcess<Airport>
{
    fn check_in(self) -> FlightProcess<Boarding>
    {
        println!("Check into the eff'n flight...");
        FlightProcess { state: Boarding }
    }
}

impl FlightProcess<Boarding>
{
    fn board_flight(self) -> FlightProcess<InFlight>
    {
        println!("Board the damn flight...");
        FlightProcess { state: InFlight }
    }
}

impl FlightProcess<InFlight>
{
    fn arrive(self) -> FlightProcess<Arrived>
    {
        println!("Arriving at the destination...");
        FlightProcess { state: Arrived }
    }
}

impl FlightProcess<Arrived>
{
    fn complete(self)
    {
        println!("You have arrived.");
    }
}

fn main()
{
   let flight_process = FlightProcess { state: AtHome };
   let packed_bags = flight_process.pack_bags();
   let heading_to_airport = packed_bags.head_to_airport();
   let checked_in = heading_to_airport.check_in();
   let boarded_flight = checked_in.board_flight();
   let arrived = boarded_flight.arrive();
   arrived.complete();
   
}
