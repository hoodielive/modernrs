use std::fmt;
impl fmt::Display for LockError
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
    {
        match self
        {
            Self::MechanicalError(code) => write!(f, "Mechanical Error: {}", code),
            Self::NetworkError     => write!(f, "Network Error"),
            Self::NotAuthorized    => write!(f, "Not Authorized"),
        }
    }
}

