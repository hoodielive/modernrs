#![allow(dead_code)]
use thiserror::Error;

#[derive(Debug, Error)]
enum NetworkError
{
    #[error("Connection timed out")]
    TimeOut,

    #[error("Unreachable")]
    Unreachable,
}

#[derive(Debug, Error)]
enum LockError
{
    #[error("Mechanical Error: {0}")]
    MechanicalError(i32, i32),
    
    #[error("Network Error")]
    NetworkError(#[from] NetworkError),
    
    #[error("Not Authorized")]
    NotAuthorized,
}

fn lock_door() -> Result<(), LockError>
{
    Err(LockError::NetworkError(NetworkError::TimeOut))
}

fn main() 
{
    let return_the_lock_code = lock_door();
    println!("{:?}", return_the_lock_code);
}
