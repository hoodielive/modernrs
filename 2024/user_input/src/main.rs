use std::io; // error type is baked in 
             // because so many errors 
             // can take place regarding i/o

// This function gathers user input into a String
fn get_input()->io::Result<String>
{
    // String buffer | empty string
    // Needs to be mutable because 
    // we will be modifying the buffer
    let mut buffer = String::new();

    // May possibly fail - so we add (?)
    // Borrow buffer mutably to
    // readline and save it into buffer
    // readline function returns a Result
    io::stdin().read_line(&mut buffer)?;
    
    // Return the buffer but
    // trim it to remove whitespace
    // take ownership of the buffer
    // again because trim creates
    // a str (string slice)
    Ok(buffer.trim().to_owned())
}

fn main() 
{
    // Make this mutable because it will
    // be adding data from get_input()
    let mut all_input: Vec<_> = vec![];
    
    // The number of times to input data
    let mut times_input = 0;

    while times_input < 2 
    {
        match get_input()
        {
            // So we know that our get_input function
            // returns a Result so we first match on
            // Ok(()) and then it returns Err if something
            // went wrong so we match on Err as well.
            Ok(words) => 
            {
              all_input.push(words);
              times_input += 1;
            },
            Err(e) => println!("Error: {:?}", e),
        }
    }

    for input in all_input
    {
        println!("Original: {:?}, capitialized: {:?}.", input, input.to_uppercase())
    };
}
