use std::io;

fn get_input()->io::Result<f64>
{
    println!("Enter a Celcius value: ");
    let mut value = String::new();
    
    io::stdin().read_line(&mut value)?;
    
    match value.trim().parse::<f64>()
    {
       Ok(celsius_value) =>
       {
            println!("Farenheit {} degrees", convert(celsius_value));
            Ok(celsius_value)
       },
       Err(e) => Err(io::Error::new(io::ErrorKind::InvalidInput, e)),
    }
}

fn main()
{
    let mut all_input: Vec<f64> = Vec::new();
    let mut times_input = 0;

    while times_input < 2
    {
        match get_input()
        {
            Ok(value) => 
            {
                all_input.push(value);
                times_input += 1;
            },
            Err(e) => println!("Error: {:?}", e),
        }
    }
    for input in all_input
    {
        println!("This Program take in a Celcius value {:.2} and converts it to Farenheit.\n", input);
    }

}

fn convert(celsius: f64)->f64
{
    celsius * (9.0 / 5.0) + 32.0
}
