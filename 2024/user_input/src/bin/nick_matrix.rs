use std::fmt::{self, Display};

struct Matrix( f32, f32, f32, f32 );

impl Display for Matrix
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
    {
        write!(f, "({}, {})\n({}, {})", self.0, self.1, self.2, self.3)
    }
}

fn transpose(pair: &Matrix) -> Matrix
{
    let Matrix ( x1, x2, y1, y2 ) = pair;
    //let x1 = pair.0;
    //let x2 = pair.1;
    //let y1 = pair.2;
    //let y2 = pair.3;
    

    // Learn why dereferencing is needed here. 
    // Arguments were in incorrect order is the error I was getting before.
    //
    // So it follows the reference the value is pointing to .... dereferencing
    Matrix (*x1, *y1, *x2, *y2)
}

fn main()
{
    let matrix = Matrix(1.1, 1.2, 2.1, 2.2);
    println!("Matrix:\n{} \n", matrix);

    //Tranposed Matrix
    println!("Transpose:\n{}", transpose(&matrix));

}
