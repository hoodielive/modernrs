use std::io::prelude::*;
use std::io::{self, Read, Write, ErrorKind};

const DEFAULT_BUF_SIZE: usize = 8 * 1024;

pub fn copy<R: ?Sized, W: ?Sized>(reader: &mut R, writer: &mut W)->io::Result<u64>
where R: Read, W: Write
{
    let mut buf = [0; DEFAULT_BUF_SIZE];
    let mut written = 0;
    loop 
    {
        let len = match reader.read(&mut buf)
        {
            Ok(0) => return Ok(written),
            Ok(len) => len,
            Err(ref e) if e.kind() == ErrorKind::Interrupted => continue,
            Err(e) => return Err(e),
        };
        writer.write_all(&buf[..len])?;
        written += len as u64;
    }
}

fn grep<R>(target: &str, reader: R)->io::Result<()>
where R: BufRead
{
    let stdin = io::stdin();
    for line_result in stdin.lock().lines()
    {
        let line = line_result?;
        if line.contains(target)
        {
            println!("{}", line);
        }
    }
    Ok(())
}

fn main()
{
    let stdin = io::stdin()    ;
    let stdout = io::stdout();

    // I want to copy from stdin-to-stdout
    let bytes_copied = copy(&mut stdin.lock(), &mut stdout.lock()).expect("Failed to write shit.");
    println!("Copied {} bytes.", bytes_copied);

    // Use our grep to search for a string
    let target = "nick";
    grep(target, stdin.lock()).expect("Failed to search for a target.");
}
