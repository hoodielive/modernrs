use std::collections::HashMap;
use std::env::args;

fn main()
{
    let mut hm = HashMap::new();
    hm.insert(3, "Hello");
    hm.insert(5, "World");

    let _r = match hm.get(&3)
    {
        Some(v) => v,
        _ => "NOTHING",
    };

    println!("{}", _r);

    let _p = hm.get(&3).unwrap_or(&"No String");
    println!("{}", _p);

    match "3".parse::<f32>()
    {
        Ok(v) => println!("Ok = {}", v),
        Err(e) => println!("Errrrrooooorrrrrr - {}", e),
    }

    match get_args(3)
    {
        Ok(v) => println!("Ok = {}", v),
        Err(e) => println!("Errrrrooooorrrrrr - {}", e),
    }
}

fn get_args(n: usize)->Result<String, String>
{
   for (i, a) in args().enumerate() 
   {
       if i == n
       {
           return Ok(a);
       }
   }
   Err("Not enough Args".to_string())
}
