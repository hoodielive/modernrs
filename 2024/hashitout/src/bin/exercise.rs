use std::env::args;

fn process_args()
{
    for p in args()
    {
        // Take the first iteration off of the iterator
       if let Some(c) = p.chars().next()
       {
           match c
           {
               'w' | 'W' => println!("Hello {}", p),
               _ => {}
           }
       }
    }
}

fn main()
{
    process_args();    
}
