fn main()
{
    
    loopto10();
    array_loop();
}

fn loopto10()
{

    for n in 0..10
    {
        println!("Hello {}", n);
    }
}

fn array_loop()
{
    let mut v = Vec::new();
    v.push(4);
    v.push(7);
    v.push(9);

    for n in v 
    {
        println!("n={}", n)
    }
    let b = vec![4, 7, 9, 10];

    for bee in b
    {
        println!("This a better way {}", bee);
    }
}

fn array_loop_2()
{
    let v = vec![4, 7, 8, 9, 11, 10];

    'outer: for i in 0..10
    {
        for n in &v
        {
            if i+n == 11
            {
                break 'outer;
            }
            println!("{}", n);
        }
    }
}
