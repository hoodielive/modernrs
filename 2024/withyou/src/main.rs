fn main()
{
   let _b = highest(4, 2, 8);
   println!("Hello, {} is the highest of all 3.", _b);

   let _c = other(2, 5);
   println!("Its, {}.", _c);
}

fn highest(_a: i32, _b: u32, _c: i8)->i32 
{
    let mut res = _a;
    
    if _b as i32 > res 
    {
        res = _b as i32;
    }

    if _c as i32 > res
    {
        res = _c as i32;
    }

    res 
}

fn other(a: i32, b: i32)->i32
{
    let mut c = a + b;
    c = c % 4;
    c = c / 2;
    c
}
