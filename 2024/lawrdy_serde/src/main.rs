use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
struct Author
{
    first: String,
    last: String,
}

fn main() 
{
    let nick_wise = Author
    {
        first: "Olorunyemi".to_owned(),
        last: "Wise".to_owned(),
    };

    let serialized_json = serde_json::to_string(&nick_wise).unwrap();
    println!("Serialized to json: {}", serialized_json);

    let serialized_mp = rmp_serde::to_vec(&nick_wise).unwrap();
    println!("Serialized to MessagePack: {:?}", serialized_mp);

    let deserialized_json: Author = serde_json::from_str(&serialized_json).unwrap();
    println!("Deserialized from JSON: {:?}", deserialized_json);
    
    let deserialized_mp: Author = rmp_serde::from_read_ref(&serialized_mp).unwrap();
    println!("Deserialized from MessagePack: {:?}", deserialized_mp);
}
