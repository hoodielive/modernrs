fn main()
{
   const _MY_INTEGER: i16 = 5;
   const _MY_FLOAT: f32 = 2.34;
   
   let mut _my_variable = 1.5;
   println!("{}", _my_variable);

   let sum = add(2.5, 4.2);
   println!("sum = {}", sum);
   let age = 18;
   let _is_child = if age < 18 { true } else { false };

   println!("Is child: {}", _is_child);

   let shoes_size = 39;

   let shoes_type = match shoes_size
   {
       35..=37 => "S",
       38..=40 => "M",
       41..=43 => "L",
       _ => "Not available",
   };

   let companies = ["Apples", "Amazon", "Google", "Microsoft"];
   for i in 0..companies.len()
   {
       println!("Current company: {}", companies[i]);
   }

   for company in companies.iter()
   {
       println!("Current company: {}", company);
   }

   let arr = ["Yahoo", "Apple", "Amazon", "Google"];
   
   for ele in arr.iter()
   {
       println!("{}", ele);
   }

   let middle = &arr[1..4];
   println!("{:?}", middle);
}

fn add(x: f32, y: f32)->f32
{
    x + y    
}

fn iflet(x: i32)
{
   if x == 1
   {
       println!("X is one.");
   } else if x == 2
   {
       println!("X is two.");
   } else {
       println!("X is something else.");
   }
}

fn take_anything<T>(x: T)
{
    // 
}

fn take_two_of_something<T>(x: T, y: T)
{
    // 
}

fn take_two_thing<T, U>(x: T, y: U)
{
    //
}
