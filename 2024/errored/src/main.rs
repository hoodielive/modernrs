#![allow(dead_code)]
pub struct Transaction
{
   from: String,
   to: String,
   amount: u64, 
}

fn main() 
{
    let _trans = get_transactions("test_data/transactions.json")
        // unwrap panics if a Ok(()) is not returned
        .expect("Could not load transactions."); 
}

// Result is the way to handle functions that may fail.
fn get_transactions(fname: &str)->Result<Vec<Transaction>, String>
{
    Err("No Trans".to_string())?;
    let _s = std::fs::read_to_string(fname).unwrap();
    let _t: Vec<Transaction>;
    Ok(Vec::new())
    
}
