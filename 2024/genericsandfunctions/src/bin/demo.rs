trait CheckIn
{
    fn check_in(&self);
    fn process(&self);
}

struct Pilot;
struct Passenger;
struct Cargo;

impl CheckIn for Pilot
{
    fn check_in(&self) 
    {
        println!("Checked in as pilot.")    
    }
    fn process(&self) 
    {
        println!("Pilot enters the cockpit.")    
    }
    
}

impl CheckIn for Passenger
{
    fn check_in(&self) 
    {
        println!("Checked in as passenger.")    
    }
    fn process(&self) 
    {
        println!("Passenger takes a seat.")    
    }
}

impl CheckIn for Cargo
{
    fn check_in(&self) 
    {
        println!("Cargo checked in.")    
    }
    fn process(&self) 
    {
        println!("Cargo moved to storage.")    
    }
}

fn process_item<T: CheckIn>(item: T)
{
    item.check_in();
    item.process();
}

fn main()
{
    let paulos = Passenger;
    let carlos = Pilot;
    let cargo1 = Cargo;
    let cargo2 = Cargo;
    
    process_item(paulos);
    process_item(carlos);
    process_item(cargo1);
    process_item(cargo2);
}
