#[derive(Debug)]
enum ServicePriority
{
    High,
    Standard,
}

trait Priority
{
    fn get_priority(&self)->ServicePriority;
}

#[derive(Debug)]
struct ImportantGuest;
impl Priority for ImportantGuest
{
    fn get_priority(&self)->ServicePriority
    {
        ServicePriority::High
    }
}

#[derive(Debug)]
struct Guest;
impl Priority for Guest
{
    fn get_priority(&self)->ServicePriority
    {
        ServicePriority::Standard
    }
}

fn priori<T>(guest: T)
    where T: Priority + std::fmt::Debug 
{
    println!("{:?} is {:?}.", guest, guest.get_priority());
}

fn main()
{
    let guest = Guest;
    let vip = ImportantGuest;

    priori(guest);
    priori(vip);
}

