// Generics let you write one function to work with multiple
// types of data.

// Generic functions are "bound" or "constrained" by traits
//  - only able to work with data that implements the trait.

//  3 syntaxes are available:
//  1. fn func(param: impl Trait) {}
//  2. fn func<T: Trait>(param: T) {}
//  3. fn func<T>(param: T) where T: Trait {}

trait Move 
{
    fn move_to(&self, x: i32, y: i32);
}

struct Snake;
struct Grasshopper;

impl Move for Snake
{
    fn move_to(&self, x: i32, y: i32)
    {
        println!("Slither to ({}, {})", x, y);
    }
}

impl Move for Grasshopper
{
    fn move_to(&self, x: i32, y: i32)
    {
        println!("Hop to ({}, {})", x, y);
    }
}

// fn make_move(p1: impl Trait1, p2: impl Trait2) orj
// fn make_move(thing: impl Move, x: i32, y: i32) or
// fn make_move<T>(thing: impl Move, x: i32, y: i32) 
//      where T: Move, 
// or..
fn make_move<T: Move>(thing: T, x: i32, y: i32)
{
    thing.move_to(x, y);
}

// Generic Syntax
fn main()
{
   let python = Snake {};
   let grasshopper = Grasshopper {};

   // The following function call is actually expanded as
   // fn make_move(thing: Snake, x: i32, y: i32)
   // { thing.move_to(x, y) }
   
   make_move(python, 1, 1);
   
   // whereas
   // fn make_move(thing: Grasshopper, x: i32, y: i32)
   // { thing.move_to(x, y) }
   
   make_move(grasshopper, 3, 3);
}
