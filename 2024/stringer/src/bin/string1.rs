fn main()
{
    print_type_of(&"Hi");
    print_type_of(&String::new());

    print("TESTING:123456789012345678901234567890");
}

fn print(msg: &str)
{
    println!("{}", msg);
}

fn print_type_of<T>(_: &T)
{
    println!("Type is: {}", std::any::type_name::<T>())
}
