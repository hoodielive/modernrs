fn main() 
{
    let mut s = String::from("Hello.");

    println!("S len = {}", s.len());

    for c in s.chars()
    {
        println!("{}", c);
    }

    println!("{}", greet("Boom!".to_string()));

    for c in s.bytes()
    {
        println!("{}", c);
    }

    for (i, c) in s.char_indices()
    {
        println!("{} = {}", i, c);
    }
}

fn greet(target: String)->String
{
    target
}
