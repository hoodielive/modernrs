use std::fmt::Display;
use std::fmt::Debug;

fn omits_annotations(list: &[String])->Option<&String>
{
    list.get(0)
}

fn has_annotations<'a>(list: &'a [String])->Option<&'a String>
{
    list.get(1)
}

fn print_author(author: &'static str)
{
   println!("{}", author)
}

fn print<T: Display + 'static>(message: &T)
{
    println!("{}", message)
}

fn debug<T: Debug + ?Sized>(message: &T)
{
    println!("{:?}", message)
}

fn main() 
{
    let authors = vec!["Samuel Jones".to_owned(), "Jane Pittman".to_owned()];
    let value = omits_annotations(&authors).unwrap();
    
    println!("The first author is '{}'", value);
    
    let new_value = has_annotations(&authors).unwrap();
    
    println!("The second author is '{}'", new_value);

    let author = "L A R R Y";

    print_author(author); 
    print(&author);
    debug(author);
}
