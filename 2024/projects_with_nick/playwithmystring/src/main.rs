fn my_func(s: &String)
{
    println!("{}", s);
}

fn main()
{
    let name: String = "name are labels".to_string();
    
    my_func(&name);
    my_func(&name);
}
