pub enum Orisa {
    Sango,
    Ogun,
    Osanyin,
    Ochosi,
}

pub enum Mpungo {
    Nsasi,
    Zarrabanda,
    Ngurufinda,
    Watariamba,
}

pub enum OrisaSpec {
    Number(i32),
    Name(String),
    Unknown,
}

pub enum MpungoSpecs {
    Number(i32),
    Name(String),
    Unknown,
}
