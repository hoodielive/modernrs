struct User {
    username: String,
    email: Option<String>,
    age: u32,
}

fn main() {
    let user_without_email = User {
        username: "Opie Taylor".to_string(),
        email: None,
        age: 32,
    };

    let user_with_email = User {
        username: "Shirley Hilbert".to_string(),
        email: Some("shirleyhilbert@shirleyhilbert.com".to_string()),
        age: 32,
    };

    print_user_info(&user_with_email);
    print_user_info(&user_without_email);
}

fn print_user_info(user: &User) {
    println!("Username is {}", user.username);
    match &user.email {
        Some(email) => println!("Email: {}", email),
        None => println!("Email: (not provided)"),
    }
    println!("Age: {}", user.age);
}
