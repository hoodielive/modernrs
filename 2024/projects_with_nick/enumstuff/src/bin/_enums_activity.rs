enum Color {
    Red,
    Green,
    Blue,
}

fn print_colors(colors: Color) {
    match colors {
        Color::Red => println!("This is the color red"),
        Color::Green => println!("This is the color green"),
        Color::Blue => println!("This is the color blue"),
    }
}

struct Orisa {
    name: String,
    color: String,
}

enum Flavor {
    Cherry,
    Strawberry,
    Grape,
}

struct Drink {
    flavor: Flavor,
    fluid_oz: f64,
}

fn process_drink(drink: Drink) {
    match drink.flavor {
        Flavor::Grape => println!("Grape."),
        Flavor::Strawberry => println!("Strawberry-peach."),
        _ => println!("Nothing is left so you know what it is."),
    }

    println!("oz: {:?}", drink.fluid_oz);
}

fn main() {
    let colors = Color::Red;
    print_colors(colors);

    let sango = Orisa {
        name: "Sango".to_string(),
        color: "Red and White".to_string(),
    };

    println!("The orisa here is: {}", sango.name);
    println!("His color is {}", sango.color);

    let drink_flavor = Drink {
        flavor: Flavor::Strawberry,
        fluid_oz: 16.2,
    };

    process_drink(drink_flavor);
}
