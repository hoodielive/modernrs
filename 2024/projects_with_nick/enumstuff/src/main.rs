#![allow(dead_code)]
mod mytypes;
use mytypes::{Orisa, OrisaSpec};

fn main() {
   // demo_simple_enum();
   // demo_enum_data();
  //  fucking_with_options();
   // check_for_errors();
    demo_with_nick_on_results();
}

fn demo_simple_enum() {
    println!("Nick's head Orisa is: ");

    let an_orisa: Orisa = Orisa::Ogun;

    match an_orisa {
        Orisa::Sango => println!("Sango"),
        Orisa::Ogun => println!("Ogu"),
        Orisa::Osanyin => println!("Osanyin"),
        Orisa::Ochosi => println!("Ochosi"),
    }
}

fn demo_with_nick_on_results()
{
    println!("\nDemo using the Result<T, E> enum.");

    
    let res: Result<i32, std::num::ParseIntError>;
    res = i32::from_str_radix("So", 16);

    match res 
    {
        Ok(n) => println!("Parsed str as i32: {}.", n),
        Err(e) => println!("Something went wrong: {}.", e),
    }
    
    let res2 = i32::from_str_radix("FF", 16);
    println!("Unwrapped result: {}", res2.unwrap_or(-1));
}

fn demo_enum_data() {
    println!("Now Nick wants specs of the Orisas.");

    let orisa_spec: OrisaSpec = OrisaSpec::Name("Centello Ndoki".to_string());

    match orisa_spec {
        OrisaSpec::Number(n) => println!("The number of your Orisa is {}", n),
        OrisaSpec::Name(s) => println!("The name of your Orisa is {}", s),
        OrisaSpec::Unknown => println!("Some funky error has occurred."),
    }

    let size = std::mem::size_of::<OrisaSpec>();
    println!("Btw the size of the OrisaSpec is {} bytes.", size);
}

// OPTION ENUM() unwrap_or RESULT / ERROR
fn fucking_with_options() {
    println!("\nDemo using Option<T> enum.");

    let _seconds_to_intiation: Option<u32>;
    _seconds_to_intiation = sec_of_day(23, 59, 159);

    match _seconds_to_intiation {
        Some(s) => println!("Second of the day: {}", s),
        None => println!("Second of the day: has no value"),
    }

    println!("Unwrapped sec: {}", _seconds_to_intiation.unwrap_or(0));
}

fn sec_of_day(h: u32, m: u32, s: u32) -> Option<u32> {
    if h <= 23 && m <= 59 && s <= 59 {
        let secs = h * 3600 + m * 60 + s;

        // Return Option secs.
        return Option::Some(secs);
    } else {
        return Option::None;
    }
}

fn check_for_errors() {
    //res = i32::from_str_radix(res, 16);

    let res = i32::from_str_radix("FA", 16);

    match &res {
        Ok(n) => println!("Parsed str as i32: {}", n),
        Err(e) => println!("Error occured: {}", e),
    }

    println!("\nUnwrapped result: {}", &res.unwrap_or(1))
}

fn demo_using_result_enum() {
    println!("\nDemo using the Result<T, E> enum.");

    let res: Result<i32, std::num::ParseIntError>;

    res = i32::from_str_radix("FF", 16);

    match res {
        Ok(n) => println!("Parsed str as i32: {}", n),
        Err(e) => println!("Error Occurred: {}.", e),
    }

    let res2 = i32::from_str_radix("FF", 16);
    println!("Unwrapped result: {}", res2.unwrap_or(-1));
}
