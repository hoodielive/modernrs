
#[derive(Debug)]
struct Employee {
    name: String,
}

fn print_it(data: &str) 
{
    println!("{:?}", data);
}

fn main() 
{

    let name: String = "Ihop".to_string();
    println!("{}", name);
    println!("{}", name);

    
    print_it("A string slice.");

    let owned_string = "Owned string".to_owned();
    let another_owned = String::from("Another");
    
    print_it(&owned_string);
    print_it(&another_owned);

    let emp_name = String::from("Oloronyemi");

    let emp = Employee {
        name: emp_name.to_owned(),
    };

    println!("{}", emp_name);
    println!("{:?}", emp);
}
