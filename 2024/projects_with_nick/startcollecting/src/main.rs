#![allow(unused_imports)]
use std::collections::HashMap;

fn main() 
{
    let mut a1: [i32; 5];

    a1 = [100, 101, 102, 103, 104];
    a1[0] = 1001;
    println!("{:?}", a1);
    present_a_tuple((4, 5));

    // Vectoria
   
    let mut vec1 = Vec::new();
    vec1.push(1);
    println!("It says: {:?}", vec1);
    println!("It says: {:?}", vec1.len());
    assert_eq!(vec1[0], 1);

    vec1.push(2);
    vec1.push(3);
    
    for x in &vec1 {
        println!("{x}");
    }

    let mut vec2 = vec![1, 2, 3];
    
    vec2.push(4);
    
    let vec3 = Vec::from([1, 2, 3, 4]);
    
    assert_eq!(vec2, vec3);
    

    for c in &vec2 {
        println!("This is vec2 c: {c}");
    }
    
    for dc in &vec2 {
        println!("This is vec2 dc: {dc}");
    }

    let mut stack = Vec::new();
    stack.push(1);
    stack.push(2);
    stack.push(3);
    stack.insert(1, 33);

    while let Some(top) = stack.pop() {
        println!("{top}");
    }

    let v = vec![0, 1];

    stack.extend([1, 2, 3]);
    
    println!("{:?}", stack);
    
    read_slice(&v);
    
    #[allow(unused_variables)]
   // let u: &[usize] = &v;
    let u: &[_] = &v;
    

    let opt = stack.get(1);
    println!("{:?}", opt);

    let a_vec_test = stack.is_empty();

    match a_vec_test {
        true => println!("The thing is empty."),
        false => println!("It's a lie."),
    }
}

fn read_slice(slice: &[usize]) {}

fn present_a_tuple(a: (i32, i32)) -> (i32, i32)
{
    println!("Can't change type or size {:?}", a);
    a
}
