use std::collections::HashMap;

#[derive(Hash, Eq, PartialEq, Debug)]
struct Orisa {
    name: String,
    power: String,
}

impl Orisa {
    fn new(name: &str, power: &str) -> Orisa {
        Orisa { name: name.to_string(), power: power.to_string()}
    }
}

fn main()
{
   let mut mpungos: HashMap<i32, String> = HashMap::new(); 
   
   mpungos.insert( 1, "Zarrabanda".to_string());
   mpungos.insert( 2, "Nsasi".to_string());
    
   println!("{:?} is the first mpungo of nsambi", &mpungos.values());

   mpungos.entry(3).or_insert("Centella Ndoki".to_string());
   
   println!("Whatever: {:?}", &mpungos);

   let mut orisas: HashMap<Orisa, &str> = HashMap::from([
        (Orisa::new("Sango", "Lightning"), "Analysis"),
        (Orisa::new("Oya", "Thunderstorm"), "Transformation"),
        (Orisa::new("Orunmila", "Prescience"), "Destiny"),
        (Orisa::new("Ogun", "Warfare"), "Technology"),
   ]);

   for (orisa, power) in &orisas {
       println!("{orisa:?} and {power:?}");
   }
}

