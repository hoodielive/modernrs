
fn some_func(iparam: &mut i32, sparam: &mut String) 
{
    println!("Values initially: {}, {}", iparam, sparam);

    *iparam += 10;
    sparam.push_str(" world");
    
    println!("Values afterward: {}, {}.", iparam, sparam)
}


fn main()
{
   let mut n = 42;
   let mut s = String::from("hello");

   some_func(&mut n, &mut s);

}
