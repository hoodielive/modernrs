use std::collections::HashMap;

fn main()
{

    // HashMaps
   
    let mut book_reviews = HashMap::new();
    
    book_reviews.insert(
        "The Sacred Oracle of Ifa".to_string(),
        "The Holy Bible".to_string(),
     );

    println!("This is the title of the book: {:?}.", book_reviews["The Sacred Oracle of Ifa"]);
    
  //  let an_array = [1, 2, 3];
  //  let an_array2: [i32; 3]; 
    
    //let vec = Vec::new();
    
 //   let vec2 = vec!(1, 2);
    
    let my_tuple: (i32, &str, bool) = (3, "Hello Nick", true);
    
    #[allow(unused_variables)]
    let (num, text, flag) = my_tuple;

    // In the future, will Rust allow iteration over 'different' types?
    
    println!("What is num: {num}");
    
    process_element(my_tuple.0);
    process_element(my_tuple.1);
    process_element(my_tuple.2);
    
    let long_tuple = (1, 2, 3, 4, 5, 6, 7, 8, 9,
                      10, 11, 12);

    let long_tuple_tuple = (long_tuple, long_tuple);

    process_long_tuple(("{:?}", long_tuple_tuple));
}

fn process_element<T: std::fmt::Debug>(element: T) {
    println!("{:?}", element);
}

fn process_long_tuple<T: std::fmt::Debug>(element: T) {
    println!("{:?}", element);
}
