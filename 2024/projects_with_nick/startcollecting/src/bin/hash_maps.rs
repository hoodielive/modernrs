
use std::collections::HashMap;

fn main()
{

    // HashMaps
   
    let mut book_reviews = HashMap::new();
    
    book_reviews.insert(
        String::from("The Sacred Oracle of Ifa"), "Epega",
     );

    book_reviews.insert(
        String::from("The Holy Bible"), "Jesus",
     );

    let to_find = ["The Sacred Oracle of Ifa", "The Holy Bible"];
    
    for &book in &to_find {
        match book_reviews.get(book) {
            Some(s) => println!("{book}: {s}"),
            None => println!("I am having nothing of it."),
        }
    }

    println!("{:?}", book_reviews);
}
