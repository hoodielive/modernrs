#![allow(dead_code)]
#![allow(unused_variables)]

mod demo_locals;
mod static_local;

fn main() 
{
   demo_locals::do_it(); 
   static_local::do_it();
}
