use once_cell::sync::Lazy;
use chrono::{DateTime, Utc};

use std::thread::sleep;
use std::time::Duration;

pub fn do_it() 
{
    println!("\nIn the demo_static_local::do_it()");

    static_int_at_compile_time();
    static_int_at_run_time();
}

fn static_int_at_compile_time()
{
   static MESSAGE: &str = "Orisa is Orisa, Mpungo is Mpungo." ;
   println!("MESSAGE: {}", MESSAGE);
}

fn static_int_at_run_time()
{
    println!("Current time: {}", Utc::now().format("%T"));

    static TIMESTAMP: Lazy<DateTime<Utc>> = Lazy::new(|| {
        sleep(Duration::new(5, 0));
        let now: DateTime<Utc> = Utc::now();
        println!("Current time: {}", now.format("%T"));
        now
    });

    println!("TIMESTAMP: {}", (*TIMESTAMP).format("%T"));
}
