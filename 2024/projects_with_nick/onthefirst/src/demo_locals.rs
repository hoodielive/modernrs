pub fn do_it()
{
    println!("\nIn demo_locals::do_it()");

    let x: i32 = 42;

    if x!= 0 {
        let s1: &str = "Tata";
        println!("s1: {}", s1)
    }
}
