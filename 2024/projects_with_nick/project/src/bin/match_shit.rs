fn main()
{
    let some_boolean = true;

    match some_boolean {
        true => println!("Its true"),
        false => println!("Its false"),
    } 

    let some_int = 1;

    match some_int {
        1 => println!("Its 1."),
        2 => println!("Its 2."),
        _ => println!("You are out of alignment."),
    }

    let mut value = 1;
    loop {
        if value == 4 {
            println!("I just want to be better.");
            break;
        }
        value += 1;
    }
}
