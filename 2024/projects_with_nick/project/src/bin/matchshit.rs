//Component, Entity, System

enum Player 
{
    Demon,
    Wizard,
    Palero
}

struct Power 
{
    fire: Player,
    _span: f64,
}

fn determine_power(power: Power) 
{
    match power.fire 
    {
        Player::Demon => println!("fire: mind-control"),
        Player::Wizard => println!("cold: freeze bending"),
        Player::Palero => println!("conjuration: roots"),
    }
    println!("power: {:?}", power._span);
}

fn print_message(gt_100: bool)
{
    match gt_100 {
        true => println!("Its big!"),
        false => println!("Its small!"),
        
    }
}

fn coordinate() -> (i32, i32) {
    (1, 7)
}

fn main()
{
    let value = 200;
    let is_gt_100 = value > 100;

    print_message(is_gt_100);

    let (x, y) = coordinate();
    
    if y > 5 
    {
        println!(">5");
    } 
    else if y < 5 
    {
        println!("<5");
    } else {
        println!("=5");
    }
}
