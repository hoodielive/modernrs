fn main()
{
    let mut s1 = String::new(); // You are an owned string.
    
    s1.push_str("bar") ;

    println!("{:?}", s1);

    s1.push_str("!");
    
    println!("{:?}", s1);
    println!("{:p}", &s1);

    let s2: &str = " world";

    let s3 = s1 + &s2; // s3 moves s1 and ends it's lifecycle.
                       // you should consider cloning but it will
                       // come with a performance cost.
    
    println!("{:?}", s3);
   // println!("{:?}", s1); // this won't work. 
   
    let s4 = format!("{}-{}", s2, s3);
    println!("{:?}", s4);

    let s5 = String::from("hello");
    let slice = &s5[0..2];
    println!("Here it is: {:?}", slice);

    // iterate over strings.
    for c in "twoman".chars()
    {
        println!("{}", c)
    }

    for c in "little".bytes()
    {
        println!("lame: {}", c)
    }

    let a2 = String::from("     hello      ");
    let trimmed_a2 = a2.trim();
    println!("{}", trimmed_a2);

    let a3 = String::from("hello world");
    let replaced_a3 = a3.replace("world", "there");
    println!("{}", replaced_a3);

    let parsed_string = "3";
    let numparse: i32 = parsed_string.parse().unwrap();
    
    println!("{}", numparse);

    if parsed_string.is_empty() {
        println!("String is empty.");
    } else {
        println!("Its good.");
        println!("Its: {}", parsed_string)
    }
    
    dbg!(&parsed_string);
}


