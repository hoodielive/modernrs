// Topic: Option Combinators
// Requirements:
// * Use combinators as described in the functions Nick:
// * func01, func02, func03

// Notes:
// * Only edit func01, func02 and func03
// * Run `cargo test --bin <whatever>`

fn func01()->bool
{
    // We are checking whether or not this particular user
    // has an access level. The 'admin' user does have an
    // access level.
    // Note: Use is_some or is_none

    maybe_access("admin").is_some()
}

fn func02()->Option<Access>
{
   // Root is equivalent to Access::Admin, but it is
   // not listed in the maybe_access function.
   // Note: Use or_else and root().

    maybe_access("root").or_else(|| root())
}

fn func03()->Access
{
    // "Ose" is not a listed user, so she will be a guest.
    // Note: use unwrap_or_else

    maybe_access("Ose").unwrap_or_else(|| Access::Guest)
}

#[derive(Debug, Eq, PartialEq)]
enum Access
{
    Admin,
    User,
    Guest
}

fn maybe_access(name: &str)->Option<Access>
{
    match name
    {
        "admin" => Some(Access::Admin),
        "ngurufinda" => Some(Access::User),
        _ => None,
    }
}

fn root()->Option<Access>
{
    Some(Access::Admin)
}


fn main()
{
    #[cfg(test)]
    mod test
    {
        use crate::*;
        
        #[test]
        fn check_func_01()
        {
            assert_eq!(func01(), true, "Admins have an access level.");
        }
        
        #[test]
        fn check_func_02()
        {
            assert_eq!(func02(), Some(Access::Admin), "Root users have ADMIN access level.");
        }
        
        #[test]
        fn check_func_03()
        {
            assert_eq!(func03(), Access::Guest, "Ose is a guest.");
        }
    }
}
