
fn _loop()
{
    let numbers = vec![1, 2, 3, 4, 5];
    let mut plus_one = vec![];

    for num in numbers 
    {
        plus_one.push(num + 1);
        println!("It is now: {}", num);
    }
}

fn _iter()
{
    let numbers = vec![1, 2, 3, 4, 5];
    
    let _plus_one: Vec<_> = numbers 
        .iter()
        .map(|num| num + 1)
        .collect();

    println!("In my mapping, I found: {:?}", _plus_one);
}

fn _filter()
{
    let numbers = vec![1, 2, 3, 4, 5];

    let _plus_one: Vec<_> = numbers
        .iter()
        .filter(|&num| *num <= 3)
        .collect();

    println!("I'm returning my filter. It is: {:?}", _plus_one);
}

fn _find()
{
    let numbers = vec![1, 2, 3, 4, 5];
    let _find_me: Option<&i32> = numbers
        .iter()
        .find(|&&num| num == 3);

    println!("What did you find?: {:?}", _find_me);
}

fn _find_copied()
{
    let numbers = vec![1, 2, 3, 4, 5];
    let _find_me: Option<i32> = numbers
        .iter()
        .copied()
        .find(|&num| num == 3);

    println!("Check it, its: {:?}", _find_me);
}

fn _count()
{
    let numbers = vec![1, 2, 3, 4, 5];
    let _counter = numbers
        .iter()
        .count();

    println!("Let count together: {:?}", _counter);
}

fn _last()
{
    let numbers = vec![1, 2, 3, 4, 5];
    let _last: Option<&i32> = numbers
        .iter()
        .last();

    println!("And the last one is: {:?}", _last);
}

fn _min()
{
    let numbers = vec![1, 2, 3, 4, 5];
    let _min: Option<&i32> = numbers
        .iter()
        .min();

    println!("The min number is: {:?}", _min);
}

fn _max()
{
    let numbers = vec![1, 2, 3, 4, 5];
    let _max: Option<&i32> = numbers
        .iter()
        .max();

    println!("The max number is: {:?}", _max);
}

fn _take()
{
    let numbers = vec![1, 2, 3, 4, 5];
    
    let _take_some: Vec<&i32> = numbers
        .iter()
        .take(3)
        .collect();
    
    println!("I have taken these: {:?}", _take_some);
}

fn main() 
{
    _take()
}
