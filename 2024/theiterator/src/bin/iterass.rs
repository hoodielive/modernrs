
trait Iterator
{
    type Item;
    fn next(&mut self)->Option<Self::Item>;
}

trait IntoIterator where Self::IntoIter: Iterator<Item=Self::Item>
{
    type Item;
    type IntoIter: Iterator;
    fn into_iter(self)->Self::IntoIter;
}

fn main()
{
    println!("There's:");
    let v = vec!["antimony", "arsenic", "aluminum", "selenium"];

    for element in &v
    {
        println!("{}", element);
    }
    
    let mut iterator = (&v).into_iter();
    while let Some(element) = iterator.next() {
        println!("{}", element);
    }
}
