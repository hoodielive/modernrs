#![allow(dead_code)]
#![allow(unused_imports)]

use chrono::{DateTime, Duration, Utc};
use thiserror::Error;

struct SubwayPass
{
    id: usize,
    funds: isize,
    expires: DateTime<Utc>,
}

#[derive(Debug, Error)]
enum PassError
{
    #[error("Pass has expired.")]
    PassExpired,
    
    #[error("Insufficient Funds, you need some money: {0}")]
    InsufficientFunds(isize),
    
    #[error("Read Error, something is wrong with your card bish: {0}")]
    ReadError(String),
}

fn main()
{
   let pass_status = swipe_card().and_then(|mut pass| use_pass(&mut pass, 3));
   match pass_status
   {
       Ok(_) => println!("Its okay bish.. you can boards"),
       Err(e) => println!("Error bish.. {}, go away!", e),
   }
}

fn swipe_card() -> Result<SubwayPass, PassError>
{
    Ok(
        SubwayPass
        {
           id: 0,
           funds: 200,
           expires: Utc::now() + Duration::weeks(52),
        })

}

fn use_pass(pass: &mut SubwayPass, cost: isize) -> Result<(), PassError>
{
    if Utc::now() > pass.expires
    {
        Err(PassError::PassExpired)
    }

    else
    {
        if pass.funds - cost < 0
        {
            Err(PassError::InsufficientFunds(pass.funds))
        } else {
            pass.funds == pass.funds - cost;
            Ok(())
        }
    }
}
