use thiserror::Error;

#[derive(Debug)]
pub enum EmployeePosition {
    Manager,
    RegularEmployee,
    Janitor,
}

#[derive(Debug)]
pub enum BuildingRooms {
    SafetyBox,
    ManagersOffice,
    EmployeeArea,
    Breakroom,
    Lobby,
}

pub struct EmployeeInfo {
    position: EmployeePosition,
}

#[derive(Error, Debug)]
pub enum AccessibilityError {
    #[error("You do not have access")]
    AccessDenied,
}

impl EmployeeInfo {
    pub fn new(position: EmployeePosition) -> Self {
        Self { position }
    }

    pub fn check_accessibility(&self, room: BuildingRooms) -> Result<(), AccessibilityError> {
        match &self.position {
            EmployeePosition::Manager => {
                match room {
                    BuildingRooms::SafetyBox
                    | BuildingRooms::ManagersOffice
                    | BuildingRooms::EmployeeArea
                    | BuildingRooms::Breakroom
                    | BuildingRooms::Lobby => Ok(()),
                    _ => Err(AccessibilityError::AccessDenied),
                }
            }
            EmployeePosition::RegularEmployee => {
                match room {
                    BuildingRooms::EmployeeArea | BuildingRooms::Breakroom | BuildingRooms::Lobby => Ok(()),
                    _ => Err(AccessibilityError::AccessDenied),
                }
            }
            EmployeePosition::Janitor => {
                match room {
                    BuildingRooms::Breakroom | BuildingRooms::Lobby => Ok(()),
                    _ => Err(AccessibilityError::AccessDenied),
                }
            }
        }
    }
}

fn main() {
    let manager_info = EmployeeInfo::new(EmployeePosition::Manager);
    println!("{:?}", manager_info.check_accessibility(BuildingRooms::Breakroom));

    let janitor_info = EmployeeInfo::new(EmployeePosition::Janitor);
    println!("{:?}", janitor_info.check_accessibility(BuildingRooms::SafetyBox));
}

