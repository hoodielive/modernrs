#![allow(dead_code)]
use thiserror::Error;

// If a person is not a Manager they a DeniedEntry.
// Grant access everywhere by default.
// If a person is a regular employee they have access_granted to:
//  lobby, breakroom, and regular employee area - deny them everywhere
//  they do not have access.
// If a person is a janitor - determine where they have access:
// grant them access and deny them everywhere else.
// They do not get access to Classified areas.

#[derive(Debug, Error)]
enum NetworkError
{
    #[error("Connection timed out!")]
    TimeOut,

    #[error("Unreachable")]
    Unreachable,
}

#[derive(Debug, Error)]
enum LockError
{
    #[error("Mechanical Error: {0}")]
    MechanicalError(i32, i32),

    #[error("Network Error")]
    NetworkError(#[from] NetworkError),

    #[error("Not Authorized")]
    NotAuthorized
}

fn lock_door() -> Result<(), LockError>
{
    Err(LockError::NetworkError(NetworkError::TimeOut))
}

fn main()
{
    let return_lock_code = lock_door(); 
    println!("{:?}", return_lock_code);
}
