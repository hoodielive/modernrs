use std::fmt::Display;
use std::fmt::Debug;

#[derive(Debug)]
#[allow(dead_code)]
enum TrafficLightColor
{
   Red,
   Green,
   Yellow,
}

struct TrafficLight
{
    color: TrafficLightColor,
}

#[derive(Debug)]
struct HouseLight
{
    on: bool,
}

impl TrafficLight
{
    fn new()->Self
    {
        Self {
            color: TrafficLightColor::Red,
        }
    }

    fn get_state(&self)->&TrafficLightColor
    {
        &self.color
    }
// TODO find how to map all todo(s)
    fn turn_green(&mut self)
    {
        self.color = TrafficLightColor::Green
    }
}

impl HouseLight
{
    pub fn new()->Self
    {
        Self { on: false }
    }

    pub fn get_state(&self)->bool
    {
        self.on
    }
}

impl Display for TrafficLightColor
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>)->std::fmt::Result
    {
        let color_string = match self
        {
            TrafficLightColor::Green => "green",
            TrafficLightColor::Red => "red",
            TrafficLightColor::Yellow => "yellow",
        };

        write!(f, "{}", color_string)
    }
}

impl Debug for TrafficLight
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>)->std::fmt::Result
    {
        write!(f, "{:?}", self.color)?;
        Ok(())
    }
}

impl Display for HouseLight
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>)->std::fmt::Result
    {
        write!(f, "HouseLight is {}", if self.on { "on" } else { "off" })
    }
}

fn print_state(light: &impl Light)
{
    println!("{}", light.get_name());
}

trait Light 
{
    fn get_name(&self)->&str;
}

impl Light for HouseLight
{
    fn get_name(&self)->&str { "House Light" }
}

impl Light for TrafficLight
{
    fn get_name(&self)->&str { "Traffic Light" }
}

fn main() 
{
    let _bastard = TrafficLight::new(); 
    println!("Bastards color is {:?}.", _bastard);

    let _bastard_house = HouseLight::new();
    println!("{}", _bastard_house);
}
