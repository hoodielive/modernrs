fn main() 
{
    // immutable can not be changed when variable is created.
    // mutable can be changed after variable is created.

    let diamond: &str = "diamond";
    let mut mutable_diamond: &str = "new diamond";
    mutable_diamond = "the witcher";

    println!("{}", &diamond);
    println!("{}", &mutable_diamond);
}
