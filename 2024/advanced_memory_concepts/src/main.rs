#![allow(dead_code)]
#[allow(unused_variables)]

// * Fundamentals 
// All data has a memory address.
// Addresses determine the location of data in memory.
// Offsets can be used to access adjacent addresses.
// aka: indexes/indices.

// * Stacks: 
// Data placed sequentially.
// Limited Space.
// All variables stored on the stack.
// Not all data.
// Very fast to work with: Offsets to access.

// * Heaps
// Slower than stack.
// Unlimited space (RAM/disk limits apply)
// Uses pointers
//  Pointers are a fixed size
//  usize data type
// Vectors & HashMaps stored on the heap
//  All dynamically sized collections


// Placing data on heap

struct Entry
{
    id: i32,    
}

fn main() 
{
    let data = Entry { id: 5 };
    
    // { Box means we are putting this data onto the heap. }
    // Box represents a pointer somewhere on the heap.
    
    let _data_ptr: Box<Entry> = Box::new(data);
    
    // If you want to move this data back onto the stack, 
    // then use a '*' and the data will be moved back to the
    // stack.
    
    let _data_stack = *_data_ptr;
}
