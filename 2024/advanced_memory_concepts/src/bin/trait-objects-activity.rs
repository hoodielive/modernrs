#![allow(dead_code)]

/** Trait objects
 * Summary:
 *      A contractor wants a program that can sum the cost of materials
 *      based on how many square meters are required for a job.
 *  Requirements: 
 *      Calculate multiple material types with different costs.
 *      Must be able to process a list of varying materials.
 *      Material types and cost includes:
 *          1. Carpet - $10 per square meter.
 *          2. Tile - $15 per square meter.
 *          3. Wood - $20 per square meter.
 *      Square meters must be taken into account.
 *
 *  Notes:
 *      Create a trait that can be used to retrieve the cost of a material.
 *      Create trait objects and store them in a vector for processing.
 *      Use a function to calculate the total cost.
 *      Process at least 3 different materials.
 */

trait CostRetrieval
{
    fn amount(&self) -> f64;
}

struct Carpet(f64);
impl CostRetrieval for Carpet
{
    fn amount(&self) -> f64
    {
        self.0 + 10.0 
    }
}
struct Tile(f64);
impl CostRetrieval for Tile
{
    fn amount(&self) -> f64
    {
        self.0 + 15.0
    }
}
struct Wood(f64);
impl CostRetrieval for Wood
{
    fn amount(&self) -> f64
    {
        self.0 + 20.0
    }
}



fn main()
{
    let squares: Vec<Box<dyn CostRetrieval>> = vec![Carpet, Tile, Wood];
}
