#![allow(dead_code)]

trait Sale
{
    fn amount(&self) -> f64;
}

// This is a tuple struct ()

struct FullSale(f64);

impl Sale for FullSale
{
    fn amount(&self) -> f64
    {
        // return amount as-is
        // tuple struct
        self.0 
    }
}

struct OneDollarOffCoupon(f64);
impl Sale for OneDollarOffCoupon 
{
    fn amount(&self) -> f64
    {
        self.0 - 1.0
    }
}

struct TenPercentOffPromo(f64);
impl Sale for TenPercentOffPromo
{
    fn amount(&self) -> f64
    {
        self.0 * 0.9
    }
}

fn calculate_revenue(sales: &Vec<Box<dyn Sale>>) -> f64
{
    // Sale is just a trait, not a type.
    // Traits don't have sizes so they can't
    // be properly calculated in order
    // to lay out memory correctly. But a box
    // is a usize because its a pointer. So 
    // we are able to store any amount of pointers
    // within our vector because they are all the same
    // size.

    sales.iter().map(|sale| sale.amount()).sum()
}

fn main()
{
    let price = 20.0;
    let _regular = Box::new(FullSale(price));
    let _coupon = Box::new(OneDollarOffCoupon(price));
    let _promo = Box::new(TenPercentOffPromo(price));
    
    let _sales: Vec<Box<dyn Sale>> = vec![_regular, _coupon, _promo];
    let _revenue = calculate_revenue(&_sales);
    println!("Total Revenue = {}", _revenue);
}
