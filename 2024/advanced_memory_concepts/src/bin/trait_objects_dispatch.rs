#![allow(dead_code)]
#![allow(unused_variables)]

// Dynamically allocated object:
//  Runtime generics:
//      More flexible than generics.
//      Dynamic dispatch vs Static Dispatch.
// Allows mixed types in a collection:
//  Easier to work with similar data types.
//  Polymorphic program behavior:
//      Dynamically change program behavior at runtime.
//      Easily add new behaviors just by creating a new struct.
// Small performance penalty.

trait Clicky
{
    fn click(&self);
}

struct Keyboard;

impl Clicky for Keyboard
{
    fn click(&self)
    {
        println!("Click Clack")
    }
}

// Trait object parameter - Borrow
fn borrow_clicky(obj: &dyn Clicky)
{
    obj.click();
}

// Trait object parameter - Move
fn move_clicky(obj: Box<dyn Clicky>)
{
    obj.click();
}

// Heterogenous Vector
// Contains multiple types instead of 1 type per vector 

struct Mouse; 
impl Clicky for Mouse 
{
    fn click(&self)
    {
        println!("Click");
    }
}

fn make_clicks(clickeys: Vec<Box<dyn Clicky>>)
{
    for clicker in clickeys
    {
       clicker.click(); 
    }
}

fn main()
{
   // First way to create a trait object
    
   let keeb = Keyboard;
   borrow_clicky(&keeb);
   
   // Because of the borrow above, 
   // you no longer need the following:
   // Notice all those requirements are
   // fulfilled with the borrow params.

   let keeb_obj: &dyn Clicky = &keeb;

   // Second way
   // Not a good way to accomplish this.. 
   
   let keeb: &dyn Clicky = &Keyboard;

   // Third way
   // Box lets you move things around.
   // This is the most efficient way.
   // Essentially, put the Keyboard in a box.
   
   let keeb: Box<dyn Clicky> = Box::new(Keyboard);
   move_clicky(keeb);

   let mouse: Box<dyn Clicky> = Box::new(Mouse);
   let clickers = vec![&keeb, &mouse];

   // or 
//   let clickers_2: Vec<Box<dyn Clicky>> = vec![keeb, mouse];
}

