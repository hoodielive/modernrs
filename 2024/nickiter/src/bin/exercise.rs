// Topic: Iterator(s)
// Requirement:
// * Triple the value of each item in a vector.
// * Filter the data to only include values > 10.
// * Print out each element using a for loop.
// Note:
// * Use an iterator chain to accomplish this task Nick.o

fn _lets_go()
{
    let data: Vec<_> = vec![1, 2, 3, 4, 5]
        .iter()
        .map(|num| num * 3)
        .filter(|num| num > &10)
        .collect();
    
    for _num in data
    {
        println!("{:?}", _num);
    }
}

// My only gift to you:
fn main()
{
   _lets_go() 
}
