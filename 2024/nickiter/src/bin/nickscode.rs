#[allow(unused_variables)]
use std::fmt::Debug;

#[derive(Debug)]
struct Player {
    id: i32,
    name: String,
    class: String,
    current_xp: i32,
    items_held: Vec<Item>,
}

trait ItemsHeld {
    fn has_items(&self) -> bool;
    fn list_items(&self) -> Option<&Vec<Item>>;
    fn display_items(&self) -> ();
}

impl ItemsHeld for Player {
    fn has_items(&self) -> bool {
        if self.items_held.is_empty() {
            false
        } else {
            true
        }
    }

    fn list_items(&self) -> Option<&Vec<Item>> {
        let has_items = self.has_items();

        if has_items == false {
            None
        } else {
            Some(&self.items_held)
        }
    }

    fn display_items(&self) -> () {
        let item_list = self.list_items().unwrap();
        for items in item_list {
            println!(
                "Player Items: {:?} \nWeapon Type: {:?} \n \n ",
                items.name, items.weapon_type
            );
        }
    }
}
#[derive(Debug)]
struct Item {
    id: i32,
    name: String,
    weapon_type: String,
    xp_given: i32,
}

fn main() {
    // Creating an Item struct
    let weapon_hammer1 = Item {
        id: 1,
        name: "Pounder".to_string(),
        weapon_type: "Hammer".to_string(),
        xp_given: 32,
    };

    let weapon_hammer2 = Item {
        id: 2,
        name: "Slam dat bish ass".to_string(),
        weapon_type: "Hammer".to_string(),
        xp_given: 64,
    };

    // Pushing Item into Vector
    let mut player_items = vec![];
    player_items.push(weapon_hammer1);
    player_items.push(weapon_hammer2);

    // Constructing a main character. items_held is a Vec<Item>
    let main_character = Player {
        id: 1,
        name: "Nim".to_string(),
        class: "Archer".to_string(),
        current_xp: 0,
        items_held: player_items,
    };

    main_character.display_items();
}
