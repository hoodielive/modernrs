fn _loop()
{
    // Antiquated.
    let numbers = vec![1, 2, 3, 4, 5];

    let mut plus_one = vec![];

    for num in numbers
    {
        plus_one.push(num + 1);
        println!("It is now: {}", num);
    }
}

fn _iter()
{
    let numbers = vec![1, 2, 3, 4, 5];

    let _plus_one: Vec<_> = numbers
        .iter()
        .map(|num| num + 1)
        .collect();

    println!("I am using the fancy ass map combinator: {:?}", _plus_one);
    
}


fn _filter()
{
    let numbers = vec![1, 2, 3, 4, 5];

    let _filter_me: Vec<_> = numbers
        .iter()
        // This has to take a reference and you have
        // dereference the predictate in order to 
        // access the value of the predicate.
        .filter(|&num| *num <= 3)
        .collect();

    println!("Filtered shit: {:?}", _filter_me);
}

fn _find()
{
    let numbers = vec![1, 2, 3, 4, 5];
    let _find_me: Option<&i32> = numbers
        .iter()
        // What does &&num mean? 
        .find(|&&num| num == 3);

    println!("What did you find? {:?}", _find_me);
}

fn _find_copied()
{
    let numbers = vec![1, 2, 3, 4, 5];
    let _find_me: Option<i32> = numbers
        .iter()
        .copied()
        .find(|&num| num == 3);

    println!("Check it, its: {:?}", _find_me);
}

fn _count()
{
    let numbers = vec![1, 2, 3, 4, 5];
    // Nick keeps saying one's a LOOP.
    // Is a loop and iterator the same??
    let _counter = numbers
        .iter()
        .count();

    println!("Lets count together Nick: {:?}", _counter);
}

fn _last()
{
    let numbers = vec![1, 2, 3, 4, 5];
    let _last: Option<&i32> = numbers
        .iter()
        .last();

    println!("I will return: {:?}", _last);
}

fn _take()
{
    let numbers = vec![1, 2, 3, 4, 5];
    let _take_some: Vec<&i32> = numbers
        .iter()
        .take(3)
        .collect();

    println!("I took these: {:?}", _take_some);
}

fn _min()
{
    let numbers = vec![1, 2, 3, 4, 5];
    let _find_min: Option<&i32> = numbers.iter().min();

    println!("The minimum iteration is: {:?}", _find_min);
}

fn _max()
{
    let numbers = vec![1, 2, 3, 4, 5];
    let _find_max: Option<&i32> = numbers.iter().max();

    println!("The maximum iteration is: {:?}", _find_max);
}

fn main()
{
    _max()
    
}
