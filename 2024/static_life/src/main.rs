use std::fmt::Display;
use std::{slice::from_raw_parts, str::from_utf8_unchecked};

fn main() 
{
    
}

fn print<T: Display + 'static>(message: &T)
{
    println!("{}", message);
}

fn get_memory_location()->(usize, usize)
{
   let string = "Hello, World";
   let pointer = string.as_ptr() as usize;
   let length = string.len();
   // `string` is dropped here.
   // It's no longer accessible, but the data lives on.
   (pointer, length)
}

fn get_str_at_location(pointer: usize, length: usize)->&'static str
{
    unsafe { from_utf8_unchecked(from_raw_parts(pointer as *const u8, length)) }
}
