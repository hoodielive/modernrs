fn main()
{
   let _ = return_bish(Some(2));
}

fn return_bish(s: Option<i32> )->&'static str
{
    match s
    {
        Some(_) => "The value is Some",
        None => "The value is None",
    }
}
