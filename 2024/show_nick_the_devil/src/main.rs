fn main()
{
    let positive_example = get_message_if_positive(10);
    match positive_example{
        Some(message) => println!("{}", message),
        None => println!("No message because the number is NOT positive")
    }

    let negative_example = get_message_if_positive(-5);
    match negative_example
    {
        Some(message) => println!("{}", message),
        None => println!("No message because the number is NOT positive")
    }
}

fn get_message_if_positive(number: i32)->Option<String>
{
    if number > 0
    {
        Some(format!("The number {} is positive.", number))
    }
    else
    {
        None
    }
}
