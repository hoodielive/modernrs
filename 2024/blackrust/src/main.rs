use std::fmt::Display;
use std::collections::HashMap;

#[allow(dead_code)]
#[allow(unused_variables)]

fn main() 
{
    let a: &str = "42";
    let b: i64 = 42;

    generic_display(a);
    generic_display(b);

    let (x, y) = (4i64, 2i64);
    let point: Point<i64> = Point { x, y };

    let _imported_contacts = vec![
        Contact {
            name: "Larry".to_string(),
            email: "larry@somewhere.com".to_string(),
        },
        Contact {
            name: "Carlos".to_string(),
            email: "carlos@somewhere.com".to_string(),
        },
        Contact {
            name: "Marcus".to_string(),
            email: "marcus@somewhere.com".to_string(),
        },
    ];

    let unique_contacts: HashMap<String, Contact> = _imported_contacts
        .into_iter()
        .map(|contact| (contact.email.clone(), contact))
        .collect();

    println!("Here are the imported contacts {:#?}", unique_contacts);

    let lab = Labrador {};
    println!("{}", lab.bark());

    let husky = Husky {};
    println!("{}", husky.bark());
}


fn generic_display<T: Display>(item: T)
{
    println!("{}", item);
}


// Structs

#[derive(Debug)]
struct Contact
{
    name: String,
    email: String
}

struct Point<T> { x: T, y: T, }
struct Point2<T>(T, T);
struct Labrador{} 
struct Husky{} 


// Traits
trait Dog { fn bark(&self)->String; }

// Implementations
impl Dog for Labrador{
    fn bark(&self)->String { "wouf".to_string() }
}

impl Dog for Husky 
{ 
    fn bark(&self)->String 
    { 
        "wuuuuuuuuuuuu".to_string()
    }
}

