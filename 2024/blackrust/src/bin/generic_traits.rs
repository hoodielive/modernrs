use std::fmt::Display;
use std::fmt::Debug;
use serde::{Serialize, Deserialize};

trait Printer<S: Display>
{
    fn print(&self, to_print: S)
    {
        println!("{}", to_print);
    }
}

trait GenericPrinter<S: Display, T>
{
    fn print(&self, to_print: S)
    {
        println!("{}", to_print);
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct Point<'a, T: Debug + Clone + Serialize + Deserialize<'a>> 
{ 
    x: T, y: T, 
}

struct ActualPrinter {}
impl<S: Display> Printer<S> for ActualPrinter {}
impl<S: Display, T> GenericPrinter<S, T> for S{} 

fn main()
{
    let stringslice = "Hello";
    let nunna: i64 = 42;
    let printer = ActualPrinter{};

    printer.print(stringslice);
    printer.print(nunna);

    let peripheral_devices = vec![ 
        UsbCamera::new(), 
        UsbMicrophone::new() 
    ];
}


// Traits
trait UsbModule<S: Display + ?Sized> {}

// Structs
struct UsbCamera {}
struct UsbMicrophone {}


// Implementations
impl UsbModule  for UsbCamera {}
impl UsbCamera { fn new()->Self { } }
impl UsbModule for UsbMicrophone {}
impl UsbMicrophone { fn new()->Self { } }


