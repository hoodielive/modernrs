use std::fmt::Error;
use async_trait::async_trait;
use tokio::*;

fn main()
{
    
}

trait Module
{
    fn name(&self)->String;
    fn description(&self)->String;
}

trait SubdomainModule
{
    fn enumerate(&self, domain: &str)->Result<Vec<String>, Error>;
}

fn enumerate_subdomains<M: Module + SubdomainModule>(module: M, target: &str)
{
}

// Async traits.
#[async_trait]

trait HttpModule: Module 
{
   async fn scan(
        &self,
        http_client: &Client,
        endpoint: &str,
   )->Result<Option<HttpFinding>, Error>;
}
