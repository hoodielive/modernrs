#[derive(PartialEq, Debug)]
pub struct USD(i32);

#[derive(PartialEq, Debug)]
pub struct GBP(i32);

#[derive(PartialEq, Debug)]
pub struct CAD(i32);

pub trait ToUSDv<F>
{
    fn to_uv(&self, value: F)->f32;
}

pub trait FromUSDv<F>
{
    fn from_uv(&self, value: f32)->F;
}

pub struct Ex 
{
   cad: f32,
   gbp: f32,
}

impl ToUSDv<GBP> for Ex
{
    fn to_uv(&self, g: GBP)->f32
    {
        g.0 as f32 * self.gbp
    }
}

impl FromUSDv<CA> for Ex
{
    fn from_uv(&self, f: f32)->CAD
    {
        CAD((f / self.cad) as i32)
    }
}
#[cfg(test)]
mod tests 
{
    use super::*;

    #[test]
    fn it_works()
    {
        let g = GBP(200);
        let ex = Ex { cad: 0.7 };
        assert_eq!(2 + 2, 4);
    }
}
