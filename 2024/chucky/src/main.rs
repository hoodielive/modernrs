#![allow(dead_code)]
fn main()
{
   let _manager = Employee
   {
       position: Position::KitchenStaff,
       status: Status::Terminated,
   };

   println!("The position is: {:?}, and the status is {:?}.", _manager.position, _manager.status);
   match display_access(&_manager)
   {
       Err(e) => println!("Access is denied: {:?}", e),
       _ => (),
   }
}

#[derive(Debug)]
enum Position
{
    Maintenance,
    Marketing,
    Manager,
    LineSupervisors,
    KitchenStaff,
    AssemblyTechnicians
}

struct Employee
{
    position: Position,
    status: Status,
}

#[derive(Debug)]
enum Status
{
    Active,
    Terminated,
}

fn building_access(employee: &Employee)->Result<(), String>
{
   match employee.status
   {
       Status::Terminated =>
           return Err("You have been terminated. Did you think you continue to have access?".to_owned()),
           _ => ()
   }

   match employee.position
   {
        Position::Manager => Ok(()),
        Position::Marketing => Ok(()),
        Position::Maintenance => Ok(()),
        _ => Err("You cannot enter this building cuz. And I think you know that!".to_owned()),
   }

}

fn display_access(employee: &Employee)->Result<(), String>
{
    let _has_access = building_access(employee);
    println!("Access: {:?}", _has_access);
    Ok(())
}
