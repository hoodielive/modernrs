#![allow(dead_code)]

/** Ownership
 * Data in Rust programs have an owner:
 *  Owner is responsible for cleaning up data:
 *      Memory Management
 *  Only one owner (by default):
 *  Functions, closures, structs, enums and scopes are owners.
 * Data can be transferred (moved) from one owner to another:
 *  Function calls, variable re-assignment and closures
 * Possible to "borrow" data from an owner:
 *  Owner retains responsiblity for clean up.
 */

/** Lifetimes are a form generic so we use the generic syntax:
 *  IT is a convention to use 'a, 'b, 'c
 *  Life annotations indicate that there exists some 'owned' data that:
 *      "lives at least as long" as the borrowed data.
 *      "Outlives or outlasts" the scope of a borrow.
 *      "Exists longer than" the scope of a borrow.
 *  Structures utilizing borrowed data must: 
 *      Always be created after the owner was created.
 *      Always be destroyed before the owner is destroyed.
 * static is reserved: 'static data stays in memory until the program terminates.
 * Lifetimes allow:
 *   Borrowed data in a struct
 *   Returning references from functions
 * Lifetimes are the mechanism that tracks how long a piece of data resides in memory.
 * Lifetimes are usually elided, but can be specified manually.
 * Lifetimes will be checked by the compiler.
*/ 

#[derive(Debug)]
struct DataType;

#[derive(Debug)]
struct Name<'a>
{
    field: &'a DataType,
}

#[derive(Debug)]
enum FrozenItem
{
    IceCube,
}

#[derive(Debug)]
struct Freezer
{
    contents: Vec<FrozenItem>,
}

#[derive(Debug)]
enum Part
{
    Bolt,
    Panel,
}

#[derive(Debug)]
struct RobotArm<'a>
{
    part: &'a Part,
}

#[derive(Debug)]
struct AssemblyLine
{
    parts: Vec<Part>,
}

fn place_item(freezer: &mut Freezer, item: FrozenItem)
{
       freezer.contents.push(item);
}

// Including lifetimes in a function:

fn name<'a>(arg: &'a DataType) -> &'a DataType 
{
   arg 
}

fn main() 
{
    let mut freezer = Freezer { contents: vec![] };
    let cube = FrozenItem::IceCube;
    place_item(&mut freezer, cube);

    let line = AssemblyLine { parts: vec![Part::Bolt, Part::Panel] };
    {
        let _arm = RobotArm
        {
            part: &line.parts[0],
        };
    }
}
