#[derive(Debug)]
struct Orisas
{
    inner: Vec<TheOrisa>
}

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd)]
enum Orisa
{
    Sango,
    Ogu,
    Oya,
    Yemoja,
    Osanyin,
    Yewa,
    Obatala
}

#[derive(Debug)]
struct TheOrisa
{
    name: Orisa,
    description: String,
}
impl TheOrisa
{
    fn new(name: Orisa, desc: String) -> Self
    {
        Self
        {
            name,
            description: desc,
        }
    }
}

fn new_orisa() -> Orisas
{
    Orisas 
    {
        inner: vec![
            TheOrisa::new(Orisa::Obatala, "Chief of the White Cloth/Purity".to_owned()),
            TheOrisa::new(Orisa::Sango, "The Power of Lightning".to_owned()),
            TheOrisa::new(Orisa::Ogu, "Spiritual Warrior/Warfare".to_owned()),
            TheOrisa::new(Orisa::Oya, "Power of the Wind and Transformation".to_owned()),
            TheOrisa::new(Orisa::Osanyin, "Medicinal Power".to_owned()),
            TheOrisa::new(Orisa::Yewa, "Decomposition".to_owned()),
            TheOrisa::new(Orisa::Yemoja, "Instaneous Healing".to_owned()),
        ]
    }    
}

#[derive(Debug)]
struct YoungOrisas<'a>
{
    inner: Vec<&'a TheOrisa>, 
}

fn main()
{
   let construct_orisas = new_orisa();  
    let young_orisas = YoungOrisas
    {
        inner: construct_orisas.inner
            .iter()
            .map(|id| id.name)
            .collect()
    };
   
   
}
