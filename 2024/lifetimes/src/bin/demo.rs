#![allow(dead_code)]

#[derive(Debug)]
struct Cards
{
    inner: Vec<IdCard>,
}

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd)]
enum City
{
    Barland,
    Bazopolis,
    Fooville,
}

#[derive(Debug)]
struct IdCard
{
    name: String,
    age: u8,
    city: City,
}
impl IdCard
{
    pub fn new(name: &str, age: u8, city: City) -> Self
    {
        Self
        {
            name: name.to_string(),
            age,
            city,
        }
    }
}

fn new_ids() -> Cards
{
    Cards {
        inner: vec![
            IdCard::new("Osa", 1, City::Fooville),
            IdCard::new("Oyeku", 1, City::Barland),
            IdCard::new("Oturupon", 1, City::Barland),
            IdCard::new("Idi", 1, City::Bazopolis),
            IdCard::new("Ogbe", 1, City::Bazopolis),
        ],
    }
}

#[derive(Debug)]
struct YoungPeople<'a>
{
    inner: Vec<&'a IdCard>,
}

fn main()
{
   let ids = new_ids(); 
   let young = YoungPeople 
   { 
       inner: ids.inner
           .iter()
           .filter(|id| id.age <= 20)
           .collect() 
   };

   println!("ids");
   for id in ids.inner.iter()
   {
       println!("{:#?}", id);
   }
   
   println!("\nyoung");
   for id in young.inner.iter()
   {
       println!("{:#?}", id);
   }
}
