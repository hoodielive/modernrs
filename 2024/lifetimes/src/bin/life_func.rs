// The lifetime annotations we are telling Rust that the borrowed 
// value that is being returned is coming from one of these variables.
// That there are not going to be any new borrows.
fn longest<'a>(short_msg: &'a str, long_msg: &'a str) -> &'a str
{
       if short_msg > long_msg
       {
           long_msg
       } 
       else
       {
           short_msg
       }
}

fn main()
{
    let short = "hello";
    let long = "this is a long message";
    println!("{}", longest(short, long));
}
