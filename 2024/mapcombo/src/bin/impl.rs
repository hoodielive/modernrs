fn main()
{
   let _add = |x| 1 + x; 
   let result = _add(2);
   dbg!("This is embarassing: {}", result);

   let _add_one: Box<dyn Fn(i32) -> i32> =
       Box::new(|x: i32| x + 1);

   let value = 10;
   let _result = _add_one(value);
   
   println!("{} plus one is {}", value, _result);

   
}



