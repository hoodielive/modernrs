use std::collections::HashMap;

fn main()
{
    // You can test an Option with:
    // * .is_some() or *.is_none()
    // * you can .unwrap() which will panic! if its None.
    // * you can safely unwrap with .unwrap_or(default_value).
    
   // hasher(); 
   hasher_unwrap();
}


fn hasher()
{
    // To store arbritrary data by 'key', use a HashMap.
    // Returns Option->Some->or->None
    
    let mut map = HashMap::new();
    map.is_empty();
    map.insert("key1", "value1");
    map.insert("key2", "value2");

    println!("{:?}", map.get("key1"));
    println!("{:?}", map.get("key3"));
}

fn hasher_unwrap()
{
    let mut map = HashMap::new();

    // Note map takes HashMap<&str, &str> not 
    // HashMap<String, String>
 
    map.insert("key1", "value1");

    // Note: .get doesn't return an owned value, it returns
    // a borrowed value. If it returned an owned value, it
    // would have to give up ownership (which means) removing
    // the value from the map or it would need to clone it.
    // Cloning means extra cycles and memory which is something
    // Rust will never do for you automatically. So you get a
    // reference to your value, which was already a reference.
    // A reference to a &str has a type of &&str.
    
    println!("{}", map.get("key2").unwrap_or(&"Bish.."));

    // Note: .unwrap_or() needs to produce the exact same type as
    // The Option's type. In this case, the option's type is Option<&&str>.
    // This is to say, the Option can either be a Some(&&str)->return type of
    // .get() or None. So we need our .unwrap_or() to return a &&str which
    // means to pass it a &&str, or &"".
}

