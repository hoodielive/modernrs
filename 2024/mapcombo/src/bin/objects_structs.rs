fn main()
{
   // let light = TrafficLight { color: "red".to_owned() };
    let light = TrafficLight::new();
    println!("{}", light);
    
}

#[derive(Debug)]
struct TrafficLight
{
    color: String,
}

impl TrafficLight
{
    pub fn new()->Self
    {
        Self { color: "red".to_owned() }
    }
}
