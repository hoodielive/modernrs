// Topic: Map Combinator
// Requirements:
// Given a user name, create and print out a User Struct if the
// user exists.

// Use the existing find_user function to locate a user.
// Use the map function to create the User.
// Print out the User Struct if found, or a "not found" message if not. 

#[derive(Debug)]
struct User 
{
    _user_id: i32,
    _name: String,
}

fn find_user(name: &str)->Option<i32>
{
    let name = name.to_lowercase();
    match name.as_str()
    {
        "oya" => Some(1),
        "osa" => Some(5),
        "oji" => Some(9),
        _ => None,
    }
}

fn main() 
{
    let username = "oji";

    let user = find_user(username).map(
        |_user_id| User
        {
            _user_id,
            _name: username.to_owned(),
        });

    match user
    {
        Some(user) => println!("{:?}", user),
        None => println!("User is not found damnit!"),
        
    }
}
