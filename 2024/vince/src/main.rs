#[allow(unused_variables)]
fn main() 
{
    // Variables
    // str (string slice)
    // String 
    // Ownership and borrowing.
    let name = "Vincent"; // string slice (str)
    let name2 = String::from("Vodou"); // String
    let name3 = "Jeffrey".to_string(); // String
    let name4 = "Vincent".to_owned(); // String

    let num = 32; // i32 (32 bit integer)
    let num2 = 3.2; // f64 (64 bit float)
    
    println!("{}", name);
    print_string(&name2);
    print_string(&name2);
    let msg = print_string_implicit();
    println!("{:?}", msg);

    let x = 5;
    println!("{x}");
    let x = 6;
    println!("{x}");
}

fn print_string_implicit() -> String
{
   "I am a string".to_string()
}

fn print_string(astring: &String) -> String
{
    astring.to_owned()
}
