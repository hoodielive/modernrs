use std::{ thread, io };

fn process_files_parallel(filenames: Vec<String>) -> io::Result<()>
{
    const NTHREADS: usize = 8;
    let worklists = split_vec_into_chunks_for_nick(filenames, NTHREADS);

    let mut thread_handles = vec![];

    for worklist in worklists
    {
        thread_handles.push(thread::spawn(move || process_files(worklist)));
    }
    
    for handle in thread_handles
    {
        handle.join().unwrap()?;
    }
    Ok(())
}

fn split_vec_into_chunks_for_nick()


fn main() 
{

}
