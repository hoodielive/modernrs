enum Color
{
    Red,
    Blue,
    Green
}

fn main() 
{
    let maybe_user = Some("Nick");
    let maybe_user2 = Some("Nick");

    match maybe_user 
    {
        Some(user) => println!("user={:?} exists", user),
        None => println!("No user."),
    }

    if let Some(user) = maybe_user 
    {
        println!("user={:?}", user);
    } else {
        println!("No user with that name.");
    }

    let red = Color::Red;
    if let Color::Red = red {
        println!("It is red!");
    } else {
        println!("Its not red.");
    }
}
