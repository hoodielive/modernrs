fn main() 
{


}

fn gcd(mut n: u64, mut m: u64)->u64
{
    assert!(n != 0 && m !=0);
    while m != 0 
    {
        if m < n 
        {
            let t: u64 = m;
            m = n;
            n = t;
        }
        m = m % n;
    }
    n
}

#[test] // test marker is an attribute in rust (cargo test)
fn test_gcd()
{
    assert_eq!(gcd(14, 15), 1);
    assert_eq!(gcd(2 * 3 * 5 * 11 * 17, 3 * 7 * 11 * 19), 3 * 11);
}
