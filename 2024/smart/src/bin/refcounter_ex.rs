#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(unused_variables)]
use crate::List::{Cons, Nil};
use std::rc::Rc;

// A way to deal with a single value that have multiple owners
// rc<T> keeps track of the number of references to a value.
// When there are no more references, the value will be cleaned up.

// It is only useful in single-threaded programs.

enum List
{
    Cons(i32, Rc<List>),
    Nil,
}

fn main()
{
    // Cons own the value they hold
    let a  = Rc::new(Cons(5, Rc::new(Cons(10, Rc::new(Nil)))));
    println!("Count after creating a = {}", Rc::strong_count(&a));
   
    // b now owns a
    let b = Cons(3, Rc::clone(&a));
    
    println!("Count after creating b = {}", Rc::strong_count(&a));
    
    // c can't access a because it has already be borrowed 
    // unless you use a reference counting smart pointer.
    {
        let c = Cons(4, Rc::clone(&a));
        println!("Count after creating c = {}", Rc::strong_count(&a));
    }
    println!("Count after creating c = {}", Rc::strong_count(&a));
}
