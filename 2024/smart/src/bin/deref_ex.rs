use std::ops::Deref;

struct MyBox<T>(T);

impl<T> MyBox<T>
{
    fn new(x: T) -> MyBox<T>
    {
        MyBox(x)
    }
}

impl<T> Deref for MyBox<T> {
    type Target = T;

    fn deref(&self) -> &T {
        &self.0
    }
}

fn main()
{
    // DeRef trait allows you to customize the 
    // behavior of the deref operator (*).
    
    let x: i32 = 5;
    // let y: &i32 = &x;
    //let y: Box<i32> = Box::new(x);
    let y: MyBox<i32> = MyBox::new(x);

    // We assert that 5 is equal to x (true),
    // Then we assert that dereferencing y is also
    // equal to 5 (true).
    
    assert_eq!(5, x);
    assert_eq!(5, *y);

    // If you get rid of the deref of y, you will get an error 
    // because you cannot compare an integer to a reference to an integer.

    let m: MyBox<String> = MyBox::new(String::from("Rust"));
    hello(&m);
    hello(&(*m)[..]);
}

fn hello(name: &str)
{
    println!("Hello, {}!", name)
}
