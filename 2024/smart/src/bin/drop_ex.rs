#![allow(dead_code)]
#![allow(unused_variables)]

// Allows you to customize what happens when a value goes
// out of scope. Always used when using a smart pointer.
// On the Box<>, we want drop to deallocate the data 
// that was stored on the heap.

struct CustomSmartPointer
{
    data: String,
}

impl Drop for CustomSmartPointer
{
    fn drop(&mut self)
    {
        println!("Dropping CustomSmartPointer with data `{}`!", self.data);
    }
}

fn main()
{
   let c: CustomSmartPointer = CustomSmartPointer 
   {
        data: String::from("My stuff."),
   };
   
   let d: CustomSmartPointer = CustomSmartPointer 
   {
        data: String::from("My other stuff."),
   };
   
   println!("CustomSmartPointers created.");
   
}
