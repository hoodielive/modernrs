#![allow(dead_code)]
#[allow(unused_imports)]
#[allow(unused_variables)]
use std::cell::RefCell;


// Mutate data even though there is immutable
// references to that data.
// This is typically disallowed by Rust's borrowing rules.
// Thus to mutate data, this pattern utilizes unsafe code
// inside a data structure to bypass these typical rules.
// Unsafe code is code that is not checked at compile time for
// memory safety.
// Even though the rules won't be enforced at compile time, you 
// can still enforce them at runtime.

fn main()
{
    let a = 5;
    let b = &mut a;

    let mut c = 10;
    let d = &c;
    *d = 20;
}
