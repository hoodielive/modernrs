#![allow(dead_code)]
pub trait Messenger
{
    // Takes an immutable reference to self
    // & message to send.
    fn send(&self, msg: &str);
}

pub struct LimitTracker<'a, T: Messenger>
{
    messenger: &'a T, // ref to generic type & because we are borrowing 'T'
                      // we have to add a lifetime annotation.
    value: usize,
    max: usize,
}

impl<'a, T> LimitTracker<'a, T>
where
    T: Messenger,
{
    // Constructor
   pub fn new(messenger: &T, max: usize) -> LimitTracker<T>
   {
       LimitTracker
       {
           messenger,
           value: 0,
           max,
       }
   }

   pub fn set_value(&mut self, value: usize)
   {
       self.value = value; 
       let _percentage_of_max: f64 = self.value as f64 / self.max as f64;

       if _percentage_of_max >= 1.0
       {
           self.messenger.send("Error: You are over quota!");
       } 
       else 
           if 
               _percentage_of_max >= 0.9
               {
                   self.messenger.send("Urgent warning: You've used up 90% of your quota!");
               }
       else 
           if 
               _percentage_of_max >= 0.75
               {
                   self.messenger.send("Warning: You've used up 75% of your quoata!");
               }
   }
}

#[cfg(test)]

mod tests
{
    use super::*;
    use std::cell::RefCell;

    struct MockMessenger
    {
        sent_messages: RefCell<Vec<String>>,
    }

    impl MockMessenger
    {
        fn new() -> MockMessenger
        {
            MockMessenger
            {
                sent_messages: RefCell::new(vec![]),
            }
        }
    }

    impl Messenger for MockMessenger
    {
       fn send(&self, message: &str) 
       {
           self.sent_messages.borrow_mut().push(String::from(message));
       }
    }
    
    #[test]
    fn it_sends_an_over_75_percent_warning_message()
    {
        let mock_messenger = MockMessenger::new();
        let mut limit_tracker = LimitTracker::new(&mock_messenger, 100);

        limit_tracker.set_value(80);
        assert_eq!(mock_messenger.sent_messages.borrow().len(), 1);
    }
}

fn main() {}
