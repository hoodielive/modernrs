// Memory Leaks 
// You can create memory that is not cleaned up.
use std::rc::RefCell;

enum List
{
    Cons(i32, RefCell<Rc<List>>),
    Nil,
}

impl List
{
    fn tail(&self) -> Option<&RefCell<Rc<List>>>
    {
        match self
        {
            Cons(_, item) => Some(item),
            Nil => None,
        }
    }
}

fn main()
{
    let a = Rc::new(Cons(5, RefCell::new(Rc::new(Nil))));
    
    println!("A initial rc count = {}", Rc::strong_count(&a));
    println!("A next item = {:?}", a.tail());
    
    let b = Rc::new(Cons(10, RefCell::new(Rc::clone(&a))));
    if let Some(link) = a.tail()
    {
        *link.borrow_mut() = Rc::clone(&b);
    }
}
