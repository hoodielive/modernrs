use List::{Cons, Nil};

fn main() 
{
    // A pointer is a general concept for a variable 
    // that stores a memory address and that memory
    // address refers/points to some data in memory.

    // The most common pointer type in rust is the
    // & as it 'borrows' the data it points to. This
    // means they do NOT have ownership over the values.
    // References do not have any special capabilities.

    // Smart pointers are data structures that
    // act like regular pointers but have extra
    // metadata and extra capabilities tacked on. 

    // One example is a reference counting smart pointer
    // that allows a peice of data to have multiple
    // owners by keeping track of owners and once there
    // is no more owners, it cleans up the data.
    
    // Smart pointers own the data they point to unlike
    // references which just 'borrow' the data.

    // Vectors and Strings are smart pointers. They own
    // data and allow you to mutate it. They store extra
    // metadata (has_capacity). String ensures data is 
    // valid UTF-8. 
    
    // SP are usually impl by using structs. But unlike reg
    // structs they impl the drop and deref traits. Deref traits
    // allows instances of your smart pointer struct to be treated like
    // references. Drop allows you to customize code to run while your
    // when an instance of your smart pointer goes out of scope.

    // Box is a smart pointer that allows you to allocate values on the heap. 
    // Best use cases is when:
    // 1. You have a data type whose size cannot be known.
    // at compile time and you want to use a value of that type in a context
    // the requires knowing the exact size.
    // 2 . When you have a large amount of data and you want to transfer ownership of
    // data (Cannot utilize Copy).
    // 3. When you own a value and you only care that the value implements a certain trait
    // rather than it being a specific type. (Trait Objects).
 
    // Store 5 on the heap
    // Storing Pointer on the Stack (b) 
    
    let b: Box<i32> = Box::new(5);
    println!("b = {}", b);

    let list: List = Cons(1, Box::new(Cons(2, Box::new(Cons(3, Box::new(Nil))))));
}

enum Message
{
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(i32, i32, i32),
}

enum List
{
    Cons(i32, Box<List>),
    Nil
}

