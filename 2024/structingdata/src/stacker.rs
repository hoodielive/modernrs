fn main()
{
    let stack_num: i32 = 32;
    let mut heap_num: Vec<i32> = vec![4, 5, 6];
    stack_function(stack_num);
    heap_function(heap_num);
    
}

fn stack_function(mut var: i32)
{
    var = 56;
    println!("The copied value of the variable have been updated to {}", var);
}

fn heap_function(var: Vec<i32>)
{
    println!("the Value of the vector inside the function is {:?}", var);
}
