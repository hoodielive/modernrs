#![allow(dead_code)]
#![allow(unused_variables)]
fn some_func(iparam: &i32, sparam: &String)
{
    let n = *iparam;
    let so = String::from("jeff").to_uppercase();
    let s = (*sparam).to_uppercase();
    println!("{} {}", n, so);
    println!("{:p} {:p}", iparam, sparam);
}

fn some_func2(iparam: &i32, sparam: &String)
{
    if *iparam >= 50 {
        println!("{} {}, PASS", *iparam, sparam.to_uppercase());
    }
    else
    {
        println!("{} {}, FAIL", *iparam, sparam.to_lowercase());
    }
}

fn some_higher_level_func()
{
    let n = 32;
    let s = String::from("hello")    ;
    some_func(&n, &s);
    some_func(&n, &s);
}

fn main() 
{
    let anint = 32;
    let arefstring = String::from("Arn");
    let bully = "Bully".to_owned();
    let anew = &(*bully);
    some_func(&anint, &bully);

    
}
