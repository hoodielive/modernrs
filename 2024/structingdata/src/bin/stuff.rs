#![allow(dead_code)]
use syntect::highlighting::Theme;
use syntect::parsing::SyntaxSet;

fn highlight_string(ss: &SyntaxSet, theme: &Theme, syntax: &str, string: &str)
{
  use syntect::easy::HighlightLines;
  use syntect::util::{as_24_bit_terminal_escaped, LinesWithEndings};

  let syn = ss
      .find_syntax_by_name(syntax)
      .expect(&format!("{} syntax should exist", syntax));
  
  let mut h = HighlightLines::new(syn, theme);

  for line in LinesWithEndings::from(string) {
      let regions = h.highlight_line(line, &ss);
      println!("{}", as_24_bit_terminal_escaped(&regions[..], false));
  }
  println!("\x1b[0m");
}

fn main()
{
    
}
