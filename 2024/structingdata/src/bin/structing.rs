fn fact(a: i32) -> (String, i32) // tuple 
{
    let mut fact = 1;
    
    for n in 1 .. a + 1
    {
        fact *= n;     
    }
   
    ("Factorial of 5: { fact }".to_string(), fact)
}

fn main()
{
    let x = fact(5);
    println!("{:?}", x);
}
