


fn main()
{
    let value = 10;

    if value > 5 {
        println!("The value is greater than 5.");
    } else if value < 5 {
        println!("The value is not greater than 5.")
    } else {
        println!("Your arithmetic is faulty.");
    }
}
