// Match adds logic to your programs (it must be exhaustive however)
// Match works on expressions - not statements, so we us ',' instead of ';'.

fn main()
{
   let some_bool = false;
   
   match some_bool {
       true => println!("Its true."),
       false => println!("Its false."),
   }

   let some_int = 120;

   match some_int {
       1 => println!("its 1"),
       2 => println!("its 2"),
       3 => println!("its 3"),
       _ => println!("it something else"),
   }
}
