fn first_name()
{
    println!("Larry");
}

fn last_name()
{
    println!("Solomon");
}

fn sum(a: i32, b: i32) -> i32 
{
    a + b
}

fn display_results(results: i32) {
    println!("{:?}", results)
}

fn main() 
{
    first_name();
    last_name();
    let result = sum(2, 2);
    display_results(result);
}
