use std::collections::HashMap;

fn main() 
{
    let mut people = HashMap::new();

    people.insert("Ofun", 21);
    people.insert("Oyeku", 58);
    people.insert("Irete", 92);
    people.insert("Ogunda", 88);

    match people.get("Ofun")
    {
        Some(age) => println!("Age = {:?}", age),
        None => println!("not found"),
    }

    for (person, age) in people.iter()
    {
        println!("Person = {}, age: {:?}", person, age);
    }
}
