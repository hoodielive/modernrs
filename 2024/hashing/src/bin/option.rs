fn main()
{
    let a: Option<i32> = Some(1);
    let a_is_some = a.is_some();
    let a_is_none = a.is_none();
    let a_mapped = a.map(|num| num * 1);
    let a_filtered = a.filter(|num| num == &1);
    let a_or_else = a.or_else(|| Some(5));
    let unwrapped = a.unwrap_or_else(|| 0);

    
    let plus_one = match maybe_num() {
        Some(num) => Some(num + 1),
        None => None,
    };
    
    let plus_one_n1 = maybe_num().map(|num| num + 1);

    let plus_one_n2 = maybe_word()
        .map(|word| word.len())
        .map(|len| len * 2);
}

fn maybe_num()->Option<i32>{Some(1)}
fn maybe_word()->Option<String>{Some(String::from("Love"))}
