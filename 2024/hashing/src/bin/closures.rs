fn main()
{
    let sum = add_fn(1, 1);
    println!("{:?}", sum);

    let add = |a: i32, b: i32| -> i32
    {
        a + b
    };

    //let add2 = |a, b| a + b;
}

fn add_fn(a: i32, b: i32)->i32
{
    a + b
}
