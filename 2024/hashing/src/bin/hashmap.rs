use std::collections::HashMap;

struct Contents
{
    content: String,
}

type Table = HashMap<String,  Vec<String>>;     

fn main()
{
    let mut table = Table::new();
    table.insert("Gesualdo".to_string(), vec!["People".to_string(), "Tenebrae Responsoria".to_string()]);
    table.insert("Caravaggio".to_string(), vec!["The Musicians".to_string(), "The Calling of St. Matthew".to_string()]);
    table.insert("Cellini".to_string(), vec!["Perseus with the head of Medusa".to_string(), "A salt cellar.".to_string()]);

    show(table);

    println!("Here it is: {:?}", new_pixel_buffer(2, 4));
    
}

fn show(table: Table) 
{
    for (artist, works) in table 
    {
        println!("Works by {}:", artist);
        for work in works
        {
            println!("   {}", work);
        }
    }
}

fn new_pixel_buffer(rows: usize, cols: usize)->Vec<u8>
{
    vec![0; rows * cols]
}
