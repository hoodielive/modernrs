#![allow(dead_code)]
struct Name<T: Trait1 + Trait2, U: Trait3>
{
    field1: T,
    field2: U,
}

struct Name2<T, U>
where
    T: Trait1 + Trait2,
    U: Trait3,
{
    field1: T,
    field2: U,
}

trait Trait1 {}
trait Trait2 {}
trait Trait3 {}

trait Seat { fn show(&self); }

// You may utilize this struct
// with any type that implements
// the Seat trait. No other types
// are permitted.
struct Ticket<T: Seat> { location: T, }

#[derive(Clone, Copy)]
enum ConcertSeat
{
    FrontRow,
    MidSection(u32),
    Back(u32),
}

impl Seat for ConcertSeat
{
    fn show(&self) { todo!() }
}

#[derive(Clone, Copy)]
enum AirlineSeat
{
    BusinessClass,
    Economy,
    FirstClass,
}

impl Seat for AirlineSeat
{
    fn show(&self)    { todo!() }
}

fn ticket_info<T: Seat>(ticket: Ticket<T>)
{
    ticket.location.show();
}
fn main() 
{
    let delta = Ticket { location: AirlineSeat::FirstClass };
    let lauryn_hill_concert = Ticket { location: ConcertSeat::FrontRow };
    ticket_info(delta);
    ticket_info(lauryn_hill_concert);
}
