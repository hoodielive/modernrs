struct Prophecy<T>
{
    field: T    
}

trait Prognostication
{
    fn say(&self); 
}

impl<T> Prognostication for Prophecy<T>
{
    fn say(&self)
    {
        println!("The word of the appetite comes to me saying..");
    }
}

impl<T: Prognostication + std::fmt::Display> std::fmt::Display for Prophecy<T>
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        write!(f, "{}", self.field)
    }
}

fn prophet<T>(msg: T)
    where T: Prognostication
{
    msg.say();
}

fn misgivings()
{
    println!("we made it.")
}

fn main()
{
    let prophecy = Prophecy { field: Prophecy { field: misgivings() } };
    prophet(prophecy)
}
