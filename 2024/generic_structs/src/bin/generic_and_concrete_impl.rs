#![allow(dead_code)]
trait Game
{
    fn name(&self)->String;
}

#[derive(Debug)]
enum BoardGame
{
    Chess,
    Monopoly,
}
impl Game for BoardGame
{
    fn name(&self)->String
    {
        unimplemented!()
    }
}

#[derive(Debug)]
enum VideoGame
{
    PlayStation,
    XBox,
}
impl Game for VideoGame
{
    fn name(&self)->String
    {
        let msg = String::from("Go to the beach afterwards Video games.");
        msg
    }
}

#[derive(Debug)]
struct PlayRoom<T: Game>
{
    game: T,
}

impl PlayRoom<BoardGame>
{
    pub fn cleanup(&self)
    {
        println!("Cleaning up.")
    }
}

fn main()
{
   let name_of_game =  PlayRoom { game: BoardGame::Monopoly };
   println!("This is this great: {:?}", name_of_game);
   name_of_game.cleanup();
}
