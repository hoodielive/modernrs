use std::net::http;

pub struct MessageApp
{
    port: u16,
}

impl MessageApp
{
    pub fn new(port: u16)->Self
    {
        MessageApp { port }
    }
    
    pub fn run(&self)->std::io::Result<()>
    {
        println!("Starting http server: 127.0.0.1:{}", self.port);
        HttpServer::new(move || {
            App::new()
                .wrap(middleware::Logger::default())
                .service(index)
        })
        .bind(("127.0.0.1", self.port))?
        .workers(8)
        .run()
    }
}

fn main() 
{

}
