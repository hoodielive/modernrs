use std::io::Write;

fn main()
{
    let mut buf: Vec<u8> = vec![];
    
    let _writer: &mut dyn Write = &mut buf;
   
    println!("How many bytes did you gain: {:?}", _writer);
}


fn say_hello<W: Write>(out: &mut W)->std::io::Result<()>
{
    out.write_all(b"hello, Nick\n")?;
    out.flush()
}
