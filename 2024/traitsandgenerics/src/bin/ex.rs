use std::io::Write;

fn main()->std::io::Result<()>
{
    let mut buf: Vec<u8> = vec![];
    buf.write_all(b"hello")?;
    buf.flush()?;

    Ok(())
}
