// <T: Ord> in this function means that 'min'
// can be used with arguments of any type that
// implements the Ord trait (ordered type).
// A requirement like this is called a 'bound',
// because it limits which types 'T' can possibly
// be. The compiler generates custom machine code
// for each type 'T' that you actually use.

fn min<T: Ord>(val01: T, val02: T)->T
{
    if val01 <= val02
    {
        val01
    }
    else
    {
        val02
    }
}


fn main()
{
    let result = min(33, 66);
    println!("Let's see: {:?}", result);

}
