use std::ops::Range;

fn main()
{
    
}
pub enum Canvas {}
impl Canvas
{
    fn write_at(canvas: &mut self)->Self
    {
        canvas
    }
}
pub struct Broom { x: i32, y: i32, height: i32 }

trait Visible
{
    fn draw(&self, canvas: &mut Canvas);
    fn hit_test(&self, x: i32, y: i32)->bool;
}

impl Broom
{
    // Helper function used by Broom::draw()

    fn broomstick_range(&self)->Range<i32>
    {
        self.y - self.height - 1 .. self.y 
    }
}

impl Visible for Broom
{
    fn draw(&self, canvas: &mut Canvas)
    {
        for y in self.y - self.height - 1 .. self.y 
        {
            canvas.write_at(self.x, y, '|');
        }
        canvas.write_at(self.x, self.y, 'M');
    }

    fn hit_test(&self, x: i32, y: i32)->bool
    {
        self.x == x
            && self.y - self.height - 1 <= y 
            && y <= self.y
    }
}


