fn main()
{
   let circle = Shape::Circle { radius: 3.14 };
   let square = Shape::Square { side: 10.0 };

   println!("{}", circle.describe());
   println!("{}", square.describe());
}

enum Shape
{
    Circle { radius: f64 },
    Square { side: f64 },

}


trait Describe 
{
    
    fn describe(&self)->String;
}

impl Describe for Shape 
{
    fn describe(&self)->String
    {
        match self
        {
            Shape::Circle { radius } => format!("A circle with a radius of {}", radius),
            Shape::Square { side } => format!("A square with a side of {}", side),
        }
    }
}
