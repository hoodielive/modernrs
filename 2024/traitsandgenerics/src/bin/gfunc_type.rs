use std::io::Write;
use std::fmt::Debug;
use std::hash::Hash;

#[allow(dead_code)]
// <W: Write> is what makes the func generic
// This is a type parameter. It means that 
// throughout the body of this function, W
// Stands for some type that implements the
// Write trait.

fn say_hello<W: Write>(out: &mut W)->std::io::Result<()>
{
    out.write_all(b"dude ooo")?;
    out.flush();
    Ok(())
}

fn top_ten<T: Debug + Hash + Eq>(values: &Vec<T>)
{
    unimplemented!()
}

struct DataSet {}

trait Mapper {}
trait Serialize {}
trait Reducer {}
trait Results {}

// Run a query on a large, partitioned data set. 
fn run_query<M: Mapper + Serialize, R: Reducer + Serialize>
    (data: &DataSet, map: M, reduce: R)->Results
    where M: Mapper + Serialize, R: Reducer + Serialize
{
    unimplemented!()
}

fn main()
{
   let v1 = (0 .. 1000).collect::<Vec<i32>>(); 
   println!("{:?}", &v1);
}
