fn main()
{
    let circle = Circle { radius: 3.14 };
    let square = Square { side: 3.55 };
    
    draw_shape_(&circle);
    draw_shape_(&square);
}

pub enum Player
{
    Wizard,
    Warlock,
    Awo,
    Buddhist
}

trait Majik
{
    fn draw_power(&self);
}

trait Drawable
{
    fn draw(&self);
}

struct Circle
{
    radius: f64,
}

struct Square
{
    side: f64,
}

impl Drawable for Circle
{
    fn draw(&self)
    {
        println!("Drawing a circle with a radius: {}", self.radius);
    }
}

impl Drawable for Square
{
    fn draw(&self)
    {
        println!("Drawing a square with a side: {}", self.side);
    }
}


fn draw_shape(shape: &impl Drawable) 
{
    shape.draw();
}

fn draw_shape_<T: Drawable>(shape: &T)
{
    shape.draw();
}
