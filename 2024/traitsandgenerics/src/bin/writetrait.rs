use std::io::Write;
use std::fs::File;
use std::io::Result;

// &mut dyn Write is a mutable ref to any value that
// implements the Write trait.

fn say_hello(out: &mut dyn Write)->Result<()>
{
    out.write_all(b"hello Directions\n")?;
    out.flush()
}

fn main()->Result<()>
{
   let mut _local_file = File::create("Hello.txt")?;

   say_hello(&mut _local_file)?;

   let mut bytes = vec![];
   say_hello(&mut bytes)?;

   assert_eq!(bytes, b"ello Directions\n");

   Ok(())
}
