fn main()
{
    let _wizard = Player::Wizard("knowledge".to_string());
    let _sorcerer = Player::Sorcerer("majik".to_string());

    println!("Wizards: {:#?}", _wizard.unique_power());
    println!("Sorcerers: {:#?}", _sorcerer.unique_power());
}

#[derive(Debug)]
enum Player
{
    Wizard(String),
    Sorcerer(String),
    Awo(String),
    Witch(String),
}

trait Powers
{
    fn unique_power(&self)->String;
}

impl Powers for Player
{
    fn unique_power(&self)->String
    {
        match self
        {
            Player::Wizard(_) => "A wizard has the power of Magical Knowledge.".to_string(),
            Player::Sorcerer(name) if name == "majik" => "A sorcerer has the power of majik.".to_string(),
            _ => "Not dealing with you right now".to_string(),
        }
    }
}
