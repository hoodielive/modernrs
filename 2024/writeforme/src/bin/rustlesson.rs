fn main()
{
    let a_string = String::from("This is not a string.");
    
    let b_string: &str = "A slice";
    
    let b = &a_string;

    println!("{}", a_string);
    
    let c = a_string;
    let sig = return_string(String::from("This is a message"));
    
    println!("{}", sig);

    let t = (12, "eggs");
    let p  = Box::new(t);
    
}


fn return_string(message: String) -> String
{
    message
}
