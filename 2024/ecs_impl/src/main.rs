fn main() 
{
    let player = Player
    {
        power: Box::new(FirePower),
    };

    let enemy = Enemy
    {
        power: Box::new(IcePower),
    };

    player.activate();
    enemy.activate();
    
}

trait Power
{
    fn activate(&self);
}

struct Player
{
    power: Box<dyn Power>,
}

impl Power for Player
{
    fn activate(&self)
    {
        println!("Player activating power!");
        self.power.activate();
    }
}

struct Enemy
{
    power: Box<dyn Power>,
}

impl Power for Enemy
{
   fn activate(&self) 
   {
       println!("Firing a blaze!");
   }
}

struct IcePower;
struct FirePower;

impl Power for IcePower
{
    fn activate(&self)
    {
        println!("Casting a freeze!");
    }
}

impl Power for FirePower
{
    fn activate(&self)
    {
        println!("Casting a Fire spell.");
    }
}
