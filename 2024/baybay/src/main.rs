fn main() 
{
}

#[derive(Debug, Clone, Copy)]
pub struct Body
{
    _x: f64,
    _y: f64,
    _z: f64,
    _mass: f64
}

fn average(a: f64, b: f64)->f64
{
    (a+b) / 2.0
}

fn average_with_mass(a: f64, b: f64, amass: f64, bmass: f64)->f64
{
    average(a * amass, b * bmass) / (amass + bmass)
}

fn merge_two_bodies(a: Body, b: Body)->Body
{
    Body
    {
        _x: average_with_mass(a._x, b._x, a._mass, b._mass),
        _y: average_with_mass(a._y, b._y, a._mass, b._mass),
        _z: average_with_mass(a._z, b._z, a._mass, b._mass),
        _mass: a._mass + b._mass,
    }
}

fn merge_all_bodies_iter(bodies: &[Body])->Body
{
    let baybay = bodies[0];
    bodies.iter().skip(1).fold(baybay, |baybay, body|
    {
        merge_two_bodies(baybay, *body)
    })
}
