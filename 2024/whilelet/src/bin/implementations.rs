
fn main()
{
    let number = Some(4);

    if let Some(4) = number
    {
        println!("It is 4.");
    } else 
    {
        println!("It is not 4.");
    }

    while let Some(num) = number.iter().next()
    {
        println!("Um.. {:?}", num)
    }

    println!("This is DONE~!")
}
