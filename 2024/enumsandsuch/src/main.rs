#[derive(Debug)]
pub struct Bed
{
    _size: i32,
    _count: u32,
}

#[derive(Debug)]
pub enum Room
{
    Kitchen(i32),
    Bedroom(Bed), 
    Lounge(i32, String),
}

fn main()
{
   use self::Room::*;
   let t = Kitchen(4);
   let _tt = Bedroom(Bed { _size: 50, _count: 2 });
   println!("Hello from the {:?}", t);

   let _v = match t 
   {
       Room::Kitchen(n) => n,
       _ => 0,
   } + 10;

   if let Kitchen(n) = t 
   {
      println!("It is a kitchen {} cupboards", n)
   }

   while let Kitchen(n) = t 
   {
       println!("Its a kitchen with {} cupboards again.", n);
   }

   if let Lounge(n, s) = t 
   {
       println!("Its a {} lounge with {} cupboards.", s, n);
   }
}
