mod data;
use data::Nkisi;

fn get_nkisi(name:  &str) -> Result<Nkisi,String> 
{
    if name == "alert"
    {
        Ok(Nkisi::new(Nkisi::Malongo))
    }
    else
    {
        Err("Unable to find nkisi".to_owned())
    }
}

fn main()
{
    let nkisi = get_nkisi("alert");
    
    match nkisi 
    {
        Ok(_)  => println!("Nkisi data located."),
        Err(e) => println!("Error: {:?}", e),
    }
}
