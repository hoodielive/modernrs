fn main()
{
    
}

macro_rules! r#try
{
    ($expr:expr $(,)?) => 
    {
        match $expr 
        {
            $crate::result::Result::Ok(val)=>val,
            $crate::result::Result::Err(err)=>
            {
                return $crate::result::Result::Err($crate::convert::From::from(err));
            }
        }
    };
}
