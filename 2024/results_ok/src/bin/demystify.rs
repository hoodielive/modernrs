use std::net::Ipv4Addr;
use std::str::FromStr;
use std::time::Instant;

#[derive(Debug)]
struct MyError<T>(T);
    
fn main()
{
    let some = return_some();
    println!("{:?}", some);
    
    let none = return_none();
    println!("{:?}", none);

    let ok = return_ok();
    println!("{:?}", ok);
    
    let errant = return_err();
    println!("{:?}", errant);

    #[allow(unused_must_use)] let _ = unwrap_you_baby();

    let default_string = "Default value".to_owned();
    
    let unwrap_or = return_none().unwrap_or(default_string);
    
    println!("return_none().unwrap_or(...): {:?}", unwrap_or);

    let unwrap_or_else = return_none().unwrap_or_else(|| 
      format!("Default value from a function at time {:?}", Instant::now()));
    
    println!("return_none().unwrap_or_else(|| {{...}}: {:?})", unwrap_or_else);

    let _my_string = maybe_none().unwrap_or_default();

    let _match_value = match return_some()
    {
        Some(val) => val, 
        None => "My default value".to_owned(),
    };

    println!("match {{...}}: {:?}", _match_value);

    if let Some(val) = return_some()
    {
        println!("if let : {:?}", val);
    }
}

struct MyStruct;

trait Default
{
   fn default()->Self; 
}

impl Default for MyStruct
{
    fn default()->Self
    {
        unimplemented!() 
    }
}

fn return_some()->Option<String>
{
    Some("Somewhere over the rainbow".to_owned())
}

fn return_none()->Option<String>
{
    None
}

fn return_ok()->Result<String, MyError<String>>
{
   Ok("This turned out great!".to_owned())
}

fn return_err()->Result<String, MyError<String>>
{
    Err(MyError("This failed horribly.".to_string()))
}

fn unwrap_you_baby()->Result<String, MyError<String>>
{
    match Ipv4Addr::from_str("127.0.0.1")
    {
       Ok(ip_address) => Ok(ip_address.to_string()),
       Err(_) => Err(MyError("Error".to_string())),
    }
}

fn maybe_none()->Result<String, MyError<String>>
{
    Ok("You are okay!".to_owned())
}

