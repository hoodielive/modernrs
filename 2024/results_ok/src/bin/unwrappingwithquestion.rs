use std::fs::read_to_string;

// Returns Nothing or may fail.

fn main()->Result<(), std::io::Error>
{
    // Unwrap the result from render_markdown with ?
    // Since there is no caller above main, an error
    // will kill our program.
    
    let html = render_markdown("./README.md")?;
    println!("{}", html);
   
    // Finish our main with Ok(()) because our return type
    // is Result<(), ...>. We don't care about the value 
    // we return, but we have a return Ok() regardless.

    Ok(())
}

// Returns either the contents of a file as a String or
// Returns an error of the type std::io::Error.

fn render_markdown(file: &str)->Result<String, std::io::Error>
{
    // Unwrap the variable source with ? operator.
    // If the result is an error, the ? returns the
    // result back to the caller.
    
    let source = read_to_string(file)?;
    Ok(markdown::to_html(&source))
}
