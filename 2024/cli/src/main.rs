extern crate structopt;
extern crate clap;

use std::path::PathBuf;
use structopt::*;
use clap::*;


fn main() 
{
    let cli = Command::new(clap::crate_name!())
        .version(clap::crate_version!())
        .about(clap::crate_decription!())
        .subcommand(Command::new("modules").about("List all modules"))
        .subcommand(
            Command::new("scan").about("Scan a target").arg(
                Arg::new("target")
                    .help("The domain name to scan")
                    .required(true)
                    .index(1)
                ),
            )
            .arg_required_else_help(true)
            .get_matches();
                
}
