#![allow(dead_code)]

#[derive(Debug, Clone, Copy)]
enum BroomIntent
{
   FetchWater,
   DumpWater
}

#[derive(Debug)]
struct GrayScaleMap
{
    pixels: Vec<u8>,
    size: (usize, usize)
}

struct Broom
{
    name: String,
    height: u32,
    health: u32,
    position: (f32, f32, f32),
    intent: BroomIntent,
}


fn main()
{
    // Construct a value of this type GrayScaleMap with
    // a struct expression:

    let width = 1024;
    let height = 576;
    
    // A struct expression starts with the type name and the
    // value of each field, all enclosed in curly braces.
    
    let image = GrayScaleMap
    {
        pixels: vec![0; width * height],
        size: (width, height)
    };

    println!("{:#?}", image);
}

// The function takes ownership of Broom
fn chop(b: Broom) -> (Broom, Broom)
{
    // Initialize `broom1` mostly `b`, changing only height.
    // Since `String` is not `Copy`, broom1 takes ownership of
    // `b`'s name.
    let mut broom1 = Broom { height: b.height / 2, ..b };
    let mut broom2 = Broom { name: broom1.name.clone(), ..broom1 };

    broom1.name.push_str(" I");
    broom2.name.push_str(" II");
    
    (broom1, broom2)
}
