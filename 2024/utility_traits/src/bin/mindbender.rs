#[derive(Debug, Copy, Clone)]
enum GoblinIntent
{
    Rip,
    Destroy,
    Devour,
}

struct Goblin
{
    name: String,
    height: u32,
    width: u32,
    health: u32,
    position: (f32, f32, f32),
    intent: GoblinIntent
}

fn devour(d: Goblin) -> (Goblin, Goblin)
{
   let mut devour1 = Goblin{ height: d.height / 2, ..d };
   let mut devour2 = Goblin{ name: devour1.name.clone(), ..devour1 };

   devour1.name.push_str(" I");
   devour2.name.push_str(" II");
   
   (devour1, devour2) 
}

fn main()
{
    
}
