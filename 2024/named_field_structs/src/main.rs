#![allow(dead_code)]
#[allow(unused_variables)]
fn main() 
{
    let width = 1024;
    let height = 576;
    let image = GrayScaleMap {
        pixels: vec![0; width * height],
        size: (width, height)
    };

    let hogwart = Broom
    {
        name: "Hogwart Legacy".to_string(),
        height: 60,
        health: 100,
        position: (100.0, 200.0, 0.0),
        intent: BroomIntent::FetchWater
    };

    let (hogwart1, hogwart2) = chop(hogwart);
    println!("{:#?}", (hogwart1, hogwart2));
}

// Structs
// They assemble several values of assorted types into a single value
// so you can deal with them as a unit. Essentially there are 3 types of 
// structs in Rust: named-field, tuple-like and unit-like.

// Named-Field, Rectangle 8 bit grayscale pixels.

struct GrayScaleMap
{
    pixels: Vec<u8>,
    size: (usize, usize),
}

#[derive(Copy, Clone, Debug)] 
enum BroomIntent { FetchWater, DumpWater }

#[derive(Debug)]
struct Broom
{
    name: String,
    height: u32,
    health: u32,
    position: (f32, f32, f32),
    intent: BroomIntent
}

// Shorthand for populating fields from local variables or arguments with the
// same name.

fn new_map(size: (usize, usize), pixels: Vec<u8>) -> GrayScaleMap
{
    assert_eq!(pixels.len(), size.0 * size.1);
    // a struct-expression
    GrayScaleMap { pixels, size }
}

fn new_broom(name: String, height: u32, health: u32, position: (f32, f32, f32), intent: BroomIntent) -> Broom
{
    Broom { name, height, health, position, intent }
}

fn chop(b: Broom) -> (Broom, Broom)
{
    let mut broom1 = Broom { height: b.height / 2, ..b };

    let mut broom2 = Broom { name: broom1.name.clone(), ..broom1 };

    broom1.name.push_str(" I");
    broom2.name.push_str(" II");

    (broom1, broom2)
}
