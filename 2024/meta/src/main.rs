
// Define a macro named bitch_better_have_my_money
macro_rules! bitch_better_have_my_money
{
    // this macro takes the name of the function to define
    ($bitch_better_have_my_money:ident) =>
    {
        // define the function with a given name
        fn $bitch_better_have_my_money(x: i32)->i32
        {
            x * x
        }
    };
}

// Now use the damn macro to define a function named money
bitch_better_have_my_money!(money);

fn main()
{
    let value = 5;
    let b_money = money(value);
    println!("The bitch amount is {} is {}", value, b_money);
}
