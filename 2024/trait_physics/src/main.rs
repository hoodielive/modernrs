trait Fall
{
    fn hit_ground(&self);
}

struct Vase;

impl Fall for Vase
{
    fn hit_ground(&self)
    {
        println!("The Vase broke.");
    }
}

struct Cat;
impl Fall for Cat
{
   fn hit_ground(&self) 
   {
       println!("The cat causally walked away.");
   }
}

fn fall(thing: impl Fall)
{
    thing.hit_ground();
}

fn _fall<T: Fall + std::fmt::Display>(thing: T)
{
    thing.hit_ground();
}

impl std::fmt::Display for Vase
{
   fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
   {
       write!(f, "Vase is the size of {}", Vase)
   }
}

fn main() 
{
    println!("{}", Vase {});
    fall(Vase {});
    fall(Cat {});
}
