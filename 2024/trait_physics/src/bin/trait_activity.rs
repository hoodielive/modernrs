trait Perimeter
{
    fn calc_perimeter(&self)->i32;
}

struct Triangle
{
    side_a: i32,
    side_b: i32,
    side_c: i32,
}

struct Square 
{
    side: i32,
}

impl Perimeter for Square
{
    fn calc_perimeter(&self)->i32
    {
        self.side * 4
    }
}

impl Perimeter for Triangle
{
   fn calc_perimeter(&self)->i32
   {
     self.side_a + self.side_b + self.side_c
   }
}

fn print_perimeter(shape: impl Perimeter)
{
    let perimeter = shape.calc_perimeter();
    println!("Perimeter = {:?}", perimeter);
}

fn main()
{
    let square = Square { side: 5 };
    let triangle = Triangle {
        side_a: 3,
        side_b: 2,
        side_c: 9,
    };
    
    print_perimeter(square);
    print_perimeter(triangle);
}

