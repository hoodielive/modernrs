struct Shapes
{
    triangle: u32,
    square: u32,
}

struct Triangle {}
struct Square {}

trait CalcTriangle
{
    fn triangle(a: u32, b: u32, c: u32)->u32;
}

trait CalcSquare
{
    fn square(uint: u32)->u32;
}

impl CalcTriangle for Triangle
{
   fn triangle(a: u32, b: u32, c: u32)->u32
   {
      a + b + c
   }
}

impl CalcSquare for Square
{
   fn square(uint: u32)->u32 
   {
        uint * 4 
   }
}

fn main()
{
    
}

