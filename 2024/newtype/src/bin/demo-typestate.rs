#![allow(dead_code)]
struct Employee<State>
{
    name: String,
    state: State,
}

impl<State> Employee<State>
{
    // If we borrowed state, the existing state would remain. 
    
    fn transition<NextState>(self, state: NextState) -> Employee<NextState>
    {
        Employee {
            name: self.name,
            state,
        }
    }

    fn read_agreement(self) -> Employee<Signature>
    {
        // We are waiting for Employee to sign agreement
        self.transition(Signature)
    }
}

struct Agreement;
impl Employee<Agreement>
{
    fn new(name: &str) -> Self
    {
        Self 
        {
            name: name.to_string(),
            state: Agreement,
        }
    }
}

struct Signature;

impl Employee<Signature>
{
    fn sign(self) -> Employee<Training>
    {
        self.transition(Training)
    }
}
struct FailedTraining { score: u8 }
struct OnboardingComplete { score: u8 }

struct Training;
#[rustfmt::skip]
impl Employee<Training>
{
    fn train(self, score: u8) -> Result<Employee<OnboardingComplete>, Employee<FailedTraining>>
    {
        if score >= 7
        {
            Ok(self.transition(OnboardingComplete { score }))
        } else {
            Err(self.transition(FailedTraining { score }))
        }
    }
}

fn main()
{
   let employee = Employee::new("Osa");
   let _onboarded = employee.read_agreement().sign().train(7);
   match _onboarded 
   {
       Ok(emp) => println!("Onboarding is now complete."),
       Err(emp) => println!("Failed training, score: {}", emp.state.score),
   }
}
