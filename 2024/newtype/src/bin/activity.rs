#![allow(dead_code)]

#[derive(Debug)]
enum Color
{
    Black,
    Blue,
    Brown,
    Custom(String),
    Gray,
    Green,
    Purple,
    Red,
    White,
    Yellow
}

// * Each new type should implement a `new` function:

#[derive(Debug)]
struct ShirtColor(Color);
impl ShirtColor
{
    fn new(color: Color) -> Result<Self, String>
    {
        match color 
        {
            Color::Purple => Err("Purple not allowed.".to_owned()),
            other => Ok(Self(other))
        }
    }
}

#[derive(Debug)]
struct ShoesColor(Color);
impl ShoesColor
{
    fn new(color: Color) -> Result<Self, String>
    {
        match color
        {
           Color::Purple => Err("Purple is not allowed.".to_owned()),
           other => Ok(Self(other))
        }
    }
}

#[derive(Debug)]
struct PantsColor(Color);
impl PantsColor
{
    fn new(color: Color) -> Result<Self, String>
    {
        match color
        {
            Color::Purple => Err("Purple is not allowed.".to_owned()),
            other => Ok(Self(other))
        }
    }
}

fn print_shirt_color(color: ShirtColor)
{
    println!("Shirt color = {:?}", color)
}

fn print_shoe_color(color: ShoesColor)
{
    println!("Shoe color = {:?}", color)
}

fn print_pants_color(color: PantsColor)
{
    println!("Pants color = {:?}", color)
}

fn main()
{
    let shirt_color = ShirtColor::new(Color::Gray);
    let pants_color = PantsColor::new(Color::Blue);
    let shoe_color = ShoesColor::new(Color::White);

    println!("The shirt color is: {:?}, while the pants color is: {:?}, with a {:?} shoe color", shirt_color, pants_color, shoe_color);

    match shirt_color
    {
       Ok(color) => print_shirt_color(color),
       Err(e) => println!("This color is wrong from some reason: {:?}", e),
    }

    match pants_color
    {
       Ok(color) => print_pants_color(color),
       Err(e) => println!("This color is wrong from some reason: {:?}", e),
    }

    match shoe_color
    {
       Ok(color) => print_shoe_color(color),
       Err(e) => println!("This color is wrong from some reason: {:?}", e),
    }
}
