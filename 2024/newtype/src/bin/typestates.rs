
struct BusTicket;
struct BoardBusTicket;
struct File<'a>(&'a str);
impl<'a> File<'a>
{
    fn read_bytes(&self) -> Vec<u8>
    {
        // ..read data..
    }

    fn delete(&self) 
    {
        // ..delete file..
    }
}

impl BusTicket
{
    fn board(self) -> BoardBusTicket
    {
        BoardBusTicket
    }
}

fn main()
{
    let ticket = BusTicket;
    let boarded = ticket.board();

    let file = File("data.txt");
    let data = file.read_bytes();
    file.delete();
}
